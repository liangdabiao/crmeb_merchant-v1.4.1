<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace app\controller\api\community;

use app\common\repositories\community\CommunityRepository;
use app\common\repositories\store\order\StoreOrderProductRepository;
use app\common\repositories\system\RelevanceRepository;
use app\common\repositories\user\UserHistoryRepository;
use app\common\repositories\user\UserRelationRepository;
use app\common\repositories\user\UserRepository;
use app\validate\api\CommunityValidate;
use crmeb\basic\BaseController;
use think\App;
use app\common\repositories\community\CommunityRepository as repository;
use think\exception\ValidateException;

class Community extends BaseController
{
    /**
     * @var CommunityRepository
     */
    protected $repository;
    protected $user;

    /**
     * User constructor.
     * @param App $app
     * @param  $repository
     */
    public function __construct(App $app, repository $repository)
    {
        parent::__construct($app);
        $this->repository = $repository;
        $this->user = $this->request->isLogin() ? $this->request->userInfo() : null;
        if (!systemConfig('community_status') ) throw  new ValidateException('未开启社区功能');
    }

    /**
     * TODO 文章列表
     * @return \think\response\Json
     * @author Qinii
     * @day 10/29/21
     */
    public function lst()
    {
        $where = $this->request->params(['keyword','topic_id','is_hot','category_id','spu_id']);
        if (!$where['category_id']) unset($where['category_id']);
        $where = array_merge($where,$this->repository::IS_SHOW_WHERE);
        [$page, $limit] = $this->getPage();
        return app('json')->success($this->repository->getApiList($where, $page, $limit, $this->user));
    }

    /**
     * TODO  关注的人的文章
     * @param RelevanceRepository $relevanceRepository
     * @return \think\response\Json
     * @author Qinii
     * @day 11/2/21
     */
    public function focuslst(RelevanceRepository $relevanceRepository)
    {
        $where = $this->repository::IS_SHOW_WHERE;
        $where_ = [
            'left_id' => $this->user->uid ?? null ,
            'type'    => RelevanceRepository::TYPE_COMMUNITY_FANS,
        ];
        $where['uids'] = $relevanceRepository->getSearch($where_)->column('right_id');
        [$page, $limit] = $this->getPage();
        return app('json')->success($this->repository->getApiList($where, $page, $limit, $this->user));
    }

    /**
     * TODO 某个用户的文章
     * @param $id
     * @return \think\response\Json
     * @author Qinii
     * @day 10/29/21
     */
    public function userCommunitylst($id)
    {
        $where = [];
        if (!$this->user || $this->user->uid !=  $id) {
            $where = $this->repository::IS_SHOW_WHERE;
        }
        $where['uid'] = $id;
        [$page, $limit] = $this->getPage();
        return app('json')->success($this->repository->getApiList($where, $page, $limit, $this->user));
    }


    /**
     * TODO 我赞过的文章
     * @param RelevanceRepository $relevanceRepository
     * @return \think\response\Json
     * @author Qinii
     * @day 10/28/21
     */
    public function getUserStartCommunity(RelevanceRepository $relevanceRepository)
    {
        [$page, $limit] = $this->getPage();
        $data = $relevanceRepository->getUserStartCommunity($this->user->uid,$page, $limit);
        return app('json')->success($data);
    }

    /**
     * @param $id
     * @return mixed
     * @author Qinii
     */
    public function show($id)
    {
        return app('json')->success($this->repository->show($id, $this->user));
    }

    /**
     * TODO 已购商品
     * @return \think\response\Json
     * @author Qinii
     * @day 10/28/21
     */
    public function payList()
    {
        [$page, $limit] = $this->getPage();
        $keyword = $this->request->param('keyword');
        $data = app()->make(StoreOrderProductRepository::class)->getUserPayProduct($keyword, $this->user->uid, $page, $limit);
        return app('json')->success($data);
    }

    /**
     * TODO 收藏商品
     * @return \think\response\Json
     * @author Qinii
     * @day 10/28/21
     */
    public function relationList()
    {
        [$page, $limit] = $this->getPage();
        $keyword = $this->request->param('keyword');
        $data = app()->make(UserRelationRepository::class)->getUserProductToCommunity($keyword, $this->user->uid, $page, $limit);
        return app('json')->success($data);
    }

    public function historyList()
    {
        [$page, $limit] = $this->getPage();
        $where['keyword'] = $this->request->param('keyword');
        $where['uid'] = $this->request->userInfo()->uid;
        $where['type'] = 1;
        $data = app()->make(UserHistoryRepository::class)->historyLst($where, $page,$limit);
        return app('json')->success($data);
    }

    /**
     * TODO 发布文章
     * @return \think\response\Json
     * @author Qinii
     * @day 10/29/21
     */
    public function create()
    {
        $data = $this->checkParams();
        $this->checkUserAuth();
        $data['uid'] = $this->request->uid();
        if (systemConfig('community_audit')) {
            $data['status'] = 1;
            $data['is_show'] = 1;
        }  else {
            $data['status'] = 0;
            $data['is_show'] = 0;
        }

        $res = $this->repository->create($data);

        return app('json')->success(['community_id' => $res]);
    }

    /**
     * TODO
     * @return bool|\think\response\Json
     * @author Qinii
     * @day 10/30/21
     */
    public function checkUserAuth()
    {
        $user = $this->request->userInfo();
        if ( systemConfig('community_auth') ) {
            if ($user->phone) {
                return true;
            }
            throw  new ValidateException('请先绑定您的手机号');
        } else {
            return true;
        }
    }


    /**
     * TODO 编辑
     * @param $id
     * @return \think\response\Json
     * @author Qinii
     * @day 10/29/21
     */
    public function update($id)
    {
        $data = $this->checkParams();
        $this->checkUserAuth();
        if(!$this->repository->uidExists($id, $this->user->uid))
            return app('json')->success('内容不存在或不属于您');
        if (systemConfig('community_audit')) {
            $data['status'] = 1;
            $data['is_show'] = 1;
        }  else {
            $data['status'] = 0;
            $data['is_show'] = 0;
        }

        $this->repository->edit($id, $data);
        return app('json')->success(['community_id' => $id]);
    }

    public function checkParams()
    {
        $data = $this->request->params(['image','topic_id','content','spu_id','order_id']);
        $data['content'] = filter_emoji($data['content']);

        app()->make(CommunityValidate::class)->check($data);

        $arr = explode("\n", $data['content']);
        $title = rtrim(ltrim($arr[0]));
        if (mb_strlen($title) > 40 ){
            $data['title'] = mb_substr($title,0,30,'utf-8');
        } else {
            $data['title'] = $title;
        }
        if ($data['image']) $data['image'] = implode(',',$data['image']);
        return $data;
    }


    /**
     * @param $id
     * @return mixed
     * @author Qinii
     */
    public function delete($id)
    {
        if (!$this->repository->uidExists($id, $this->user->uid))
            return app('json')->fail('内容不存在或不属于您');
        $this->repository->destory($id, $this->user);

        return app('json')->success('删除成功');
    }

    /**
     * TODO 文章点赞/取消
     * @param $id
     * @param RelevanceRepository $relevanceRepository
     * @return \think\response\Json
     * @author Qinii
     * @day 10/28/21
     */
    public function startCommunity($id)
    {
        $status = $this->request->param('status') == 1 ? 1 :0;
        if (!$this->repository->exists($id))
            return app('json')->fail('内容不存在');
        $this->repository->setCommunityStart($id, $this->user, $status);
        if ($status) {
            return app('json')->success('点赞成功');
        } else {
            return app('json')->success('取消点赞');
        }
    }

    /**
     * TODO 用户关注/取消
     * @param $id
     * @param RelevanceRepository $relevanceRepository
     * @return \think\response\Json
     * @author Qinii
     * @day 10/28/21
     */
    public function setFocus($id)
    {
        $id  = (int)$id;
        $status  = $this->request->param('status') == 1 ? 1 :0;
        if ($this->user->uid == $id)
            return app('json')->fail('请勿关注自己');
        $make = app()->make(UserRepository::class);
        if (!$user = $make->get($id)) return app('json')->fail('未查询到该用户');

        $this->repository->setFocus($id, $this->user->uid, $status);

        if ($status) {
            return app('json')->success('关注成功');
        } else {
            return app('json')->success('取消关注');
        }
    }

    /**
     * TODO 我的粉丝
     * @param RelevanceRepository $relevanceRepository
     * @return \think\response\Json
     * @author Qinii
     * @day 10/28/21
     */
    public function getUserFans(RelevanceRepository $relevanceRepository)
    {
        [$page, $limit] = $this->getPage();
        $fans = $relevanceRepository->getUserFans($this->user->uid, $page, $limit);
        return app('json')->success($fans);
    }

    /**
     * TODO 我的关注
     * @param RelevanceRepository $relevanceRepository
     * @return \think\response\Json
     * @author Qinii
     * @day 10/28/21
     */
    public function getUserFocus(RelevanceRepository $relevanceRepository)
    {
        [$page, $limit] = $this->getPage();
        $start = $relevanceRepository->getUserFocus($this->user->uid, $page, $limit);
        return app('json')->success($start);
    }


    /**
     * TODO 用户信息
     * @param $id
     * @return \think\response\Json
     * @author Qinii
     * @day 10/28/21
     */
    public function userInfo($id)
    {
        if (!$id)  return app('json')->fail('缺少参数');
        $data = $this->repository->getUserInfo($id, $this->user);
        return app('json')->success($data);
    }

    public function getSpuByOrder($id)
    {
        $data = $this->repository->getSpuByOrder($id, $this->request->userInfo()->uid);
        return app('json')->success($data);
    }
}
