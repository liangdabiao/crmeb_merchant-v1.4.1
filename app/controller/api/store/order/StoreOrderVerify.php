<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\controller\api\store\order;


use app\common\repositories\store\order\StoreOrderRepository;
use app\common\repositories\store\service\StoreServiceRepository;
use crmeb\basic\BaseController;
use think\App;
use think\exception\HttpResponseException;

class StoreOrderVerify extends BaseController
{
    protected $user;

    protected $service;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $merId = $this->request->route('merId');
        $user = $this->request->userInfo();
        $userInfo = $this->request->userInfo();
        $service = app()->make(StoreServiceRepository::class)->getService($userInfo->uid, $merId);
        if (!$service && $userInfo->main_uid) {
            $service = app()->make(StoreServiceRepository::class)->getService($userInfo->main_uid, $merId);
        }
        if (!$service || !$service->customer) {
            throw new HttpResponseException(app('json')->fail('没有权限'));
        }
        $this->service = $service;
        $this->user = $user;
    }

    public function detail($merId, $id, StoreOrderRepository $repository)
    {
        $order = $repository->codeByDetail($id);
        if (!$order) return app('json')->fail('订单不存在');
        if ($order->mer_id != $merId)
            return app('json')->fail('没有权限查询该订单');
        return app('json')->success($order->toArray());
    }

    public function verify($merId, $id, StoreOrderRepository $repository)
    {
        $repository->verifyOrder($id, $merId, $this->service->service_id);
        return app('json')->success('订单核销成功');
    }
}
