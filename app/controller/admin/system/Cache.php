<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------
namespace app\controller\admin\system;

use app\common\repositories\system\CacheRepository;
use crmeb\basic\BaseController;
use crmeb\services\CacheService;
use think\App;

class Cache extends BaseController
{
    /**
     * @var CacheRepository
     */
    protected $repository;

    /**
     * CacheRepository constructor.
     * @param App $app
     */
    public function __construct(App $app, CacheRepository $repository)
    {
        parent::__construct($app);
        $this->repository = $repository;
    }


    /**
     * @Author:Qinii
     * @Date: 2020/9/15
     * @return mixed
     */
    public function getAgree($key)
    {
        $allow = [
            'sys_integral_rule',
            'sys_intention_agree',
            'sys_product_presell_agree',
            'sys_user_agree',
            'wechat_menus',
            'sys_receipt_agree',
            'sys_extension_agree',
            'sys_merchant_type',
            'sys_brokerage',
            'sys_member',
            ];
        if (!in_array($key, $allow)) return app('json')->fail('数据不存在');
        $data[$key] = $this->repository->getResult($key);
        return app('json')->success($data);
    }


    /**
     * @Author:Qinii
     * @Date: 2020/9/15
     * @return mixed
     */
    public function saveAgree($key)
    {
        /**
         * sys_integral_rule 分销
         * sys_intention_agree 积分
         * sys_product_presell_agree 预售
         * sys_user_agree 用户
         * wechat_menus
         * sys_extension_agree
         */
        $allow = [
            'sys_integral_rule',
            'sys_intention_agree',
            'sys_product_presell_agree',
            'sys_user_agree',
            'wechat_menus',
            'sys_receipt_agree',
            'sys_extension_agree',
            'sys_merchant_type',
            'sys_brokerage',
            'sys_member',
        ];
        if (!in_array($key, $allow)) return app('json')->fail('KEY不存在');

        $value = $this->request->param('agree');
        $this->repository->save($key, $value);
        return app('json')->success('保存成功');
    }

    /**
     * TODO 清除缓存
     * @return \think\response\Json
     * @author Qinii
     * @day 12/9/21
     */
    public function clearCache()
    {
        $type = $this->request->param('type','');
        $id = $this->request->param('id',0);
        $tag = $this->request->param('tag',0);
        if (in_array($type, [1,2,3,4])){
            $mthod = [
                1 => 'clearAll',
                2 => 'clearMerchantAll',
                3 => 'clearSystem',
                4 => 'clearMerchant',
            ];
            CacheService::{$mthod[$type]}($id);
        }
        if (in_array($tag,[1,2,3,4,5,6,7])) {
            $const = [
                1 => CacheService::TAG_TOPIC,
                2 => CacheService::TAG_CONFIG ,
                3 => CacheService::TAG_COMMUNITY ,
                4 => CacheService::TAG_BRAND,
                5 => CacheService::TAG_CATEGORY ,
                6 => CacheService::TAG_GROUP_DATA,
                7 => CacheService::TAG_MERCHANT,
            ];
            CacheService::clearByTag($id, $const[$tag]);
        }
        return app('json')->success('清除缓存成功');
    }
}
