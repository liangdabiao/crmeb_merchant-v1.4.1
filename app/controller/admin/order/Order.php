<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\controller\admin\order;

use crmeb\basic\BaseController;
use app\common\repositories\store\ExcelRepository;
use app\common\repositories\system\merchant\MerchantRepository;
use app\common\repositories\store\order\StoreOrderRepository as repository;
use think\App;
use app\common\model\store\order\FoxpurOrder;

class Order extends BaseController
{
    protected $repository;

    public function __construct(App $app, repository $repository)
    {
        parent::__construct($app);
        $this->repository = $repository;
    }


    public function lst($id)
    {
        [$page, $limit] = $this->getPage();
        $where = $this->request->params(['date','order_sn','order_type','keywords','username','activity_type','group_order_sn','store_name']);
        $where['reconciliation_type'] = $this->request->param('status', 1);
        $where['mer_id'] = $id;
        return app('json')->success($this->repository->adminMerGetList($where, $page, $limit));
    }

    public function markForm($id)
    {
        if (!$this->repository->getWhereCount([$this->repository->getPk() => $id]))
            return app('json')->fail('数据不存在');
        return app('json')->success(formToData($this->repository->adminMarkForm($id)));
    }

    public function mark($id)
    {
        if (!$this->repository->getWhereCount([$this->repository->getPk() => $id]))
            return app('json')->fail('数据不存在');
        $data = $this->request->params(['admin_mark']);
        $this->repository->update($id, $data);
        return app('json')->success('备注成功');
    }

    public function title()
    {
        $where = $this->request->params(['type', 'date', 'mer_id','keywords','status','username','order_sn','is_trader','activity_type']);
        return app('json')->success($this->repository->getStat($where, $where['status']));
    }
    /**
     * TODO
     * @return mixed
     * @author Qinii
     * @day 2020-06-25
     */
    public function getAllList()
    {
        [$page, $limit] = $this->getPage();
        $where = $this->request->params(['type', 'date', 'mer_id','keywords','status','username','order_sn','is_trader','activity_type','group_order_sn','store_name']);
        return app('json')->success($this->repository->adminGetList($where, $page, $limit));
    }

    public function takeTitle()
    {
        $where = $this->request->params(['date','order_sn','keywords','username','is_trader']);
        $where['take_order'] = 1;
        $where['status'] = '';
        $where['verify_date'] = $where['date'];
        unset($where['date']);
        return app('json')->success($this->repository->getStat($where, ''));
    }

    /**
     * TODO 自提订单列表
     * @return mixed
     * @author Qinii
     * @day 2020-08-17
     */
    public function getTakeList()
    {
        [$page, $limit] = $this->getPage();
        $where = $this->request->params(['date','order_sn','keywords','username','is_trader']);
        $where['take_order'] = 1;
        $where['status'] = '';
        $where['verify_date'] = $where['date'];
        unset($where['date']);
        return app('json')->success($this->repository->adminGetList($where, $page, $limit));
    }

    /**
     * TODO
     * @return mixed
     * @author Qinii
     * @day 2020-08-17
     */
    public function chart()
    {
        return app('json')->success($this->repository->OrderTitleNumber(null,null));
    }

    /**
     * TODO 自提订单头部统计
     * @return mixed
     * @author Qinii
     * @day 2020-08-17
     */
    public function takeChart()
    {
        return app('json')->success($this->repository->OrderTitleNumber(null,1));
    }

    /**
     * TODO 订单类型
     * @return mixed
     * @author Qinii
     * @day 2020-08-15
     */
    public function orderType()
    {
        return app('json')->success($this->repository->orderType([]));
    }

    public function detail($id)
    {
        $data = $this->repository->getOne($id, null);
        if (!$data)
            return app('json')->fail('数据不存在');
        return app('json')->success($data);
    }

    /**
     * TODO 快递查询
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-25
     */
    public function express($id)
    {
        if (!$this->repository->getWhereCount(['order_id' => $id, 'delivery_type' => 1]))
            return app('json')->fail('订单信息或状态错误');
        return app('json')->success($this->repository->express($id,null));
    }

    public function reList($id)
    {
        [$page, $limit] = $this->getPage();
        $where = ['reconciliation_id' => $id, 'type' => 0];
        return app('json')->success($this->repository->reconList($where, $page, $limit));
    }

    /**
     * TODO 导出文件
     * @author Qinii
     * @day 2020-07-30
     */
    public function excel()
    {
        $where = $this->request->params(['type', 'date', 'mer_id','keywords','status','username','order_sn','take_order']);
        if($where['take_order']){
            $where['verify_date'] = $where['date'];
            unset($where['date']);
        }
        app()->make(ExcelRepository::class)->create($where, $this->request->adminId(), 'order',0);
        return app('json')->success('开始导出数据');

    }

    /**
     * TODO Foxpur导出文件
     * @author Qinii
     * @day 2020-07-30
     */
    public function excelFoxPur()
    {
        $where = $this->request->params(['type', 'date', 'mer_id','keywords','status','username','order_sn','is_trader','activity_type','group_order_sn','admin_id','product_id','foxpurstatus','foxpur_admin_id']);
        app()->make(ExcelRepository::class)->create($where, $this->request->adminId(), 'foxpur_order',0);
        return app('json')->success('开始导出数据');

    }

    /**
     * TODO Foxpur 候鸟导出文件
     * @author Qinii
     * @day 2020-07-30
     */
    public function excelFoxPurHouniao()
    {
        $where = $this->request->params(['type', 'date', 'mer_id','keywords','status','username','order_sn','is_trader','activity_type','group_order_sn','admin_id','product_id','foxpurstatus','foxpur_admin_id']);
        app()->make(ExcelRepository::class)->create($where, $this->request->adminId(), 'foxpur_houniao_order',0);
        return app('json')->success('开始导出数据');

    }

    /**
     * TODO FoxPur订单列表
     * @return mixed
     * @author Qinii
     * @day 2020-08-17
     */
    public function getFoxPurList()
    {
        [$page, $limit] = $this->getPage();
        $where = $this->request->params(['type', 'date', 'mer_id','keywords','status','username','order_sn','is_trader','activity_type','group_order_sn','admin_id','product_id','uid','foxpurstatus','foxpur_admin_id']);
        return app('json')->success($this->repository->foxpurGetList($where, $page, $limit));
    }

    /**
     * TODO FoxPur导出发货单
     * @return \think\response\Json
     * @author Qinii
     * @day 3/13/21
     */
    public function foxpurdeliveryExport()
    {
        $where = $this->request->params(['type', 'date', 'mer_id','keywords','status','username','order_sn','is_trader','activity_type','group_order_sn','admin_id','product_id','foxpurstatus','foxpur_admin_id']);
        app()->make(ExcelRepository::class)->create($where, $this->request->adminId(), 'foxpur_delivery',0);
        return app('json')->success('开始导出数据');
    }
//Foxpur订单列表图标数据
    public function foxpurtitle()
    {
        $where = $this->request->params(['type', 'date', 'mer_id','keywords','status','username','order_sn','is_trader','activity_type','group_order_sn','admin_id','product_id','foxpurstatus','foxpur_admin_id']);
        return app('json')->success($this->repository->foxpurGetStat($where, $where['status']));
    }

    //foxpur批量下单
    public function batchChangeGroupFoxPurForm()
    {
        $ids = $this->request->param('ids', '');
        $ids = array_filter(explode(',', $ids));
        if (!count($ids))
            return app('json')->fail('数据不存在');
        return app('json')->success(formToData($this->repository->changeGroupFoxPur($ids)));
    }


    //foxpur批量下单
    public function batchChangeGroupFoxPur(repository $groupRepository)
    {
        $order_status = (int)$this->request->param('order_status', 0);
        $ids = (array)$this->request->param('ids', []);
        $adminInfo = $this->request->adminInfo();
        //$orderer_foxpur = count($ids) - FoxpurOrder::whereIn('order_id', $ids)->count();

        if (!count($ids))
            return app('json')->fail('数据不存在');
        //if ($orderer_foxpur!=0)
            //return app('json')->fail('有部分订单状态为空');
        if ($order_status && !$groupRepository->exists($order_status))
            return app('json')->fail('状态不存在');
        $this->repository->batchChangeGroupIdFoxPur($ids, $order_status,$adminInfo);
        return app('json')->success('修改成功');
    }


}
