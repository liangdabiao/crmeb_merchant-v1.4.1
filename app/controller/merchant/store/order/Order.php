<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace app\controller\merchant\store\order;

use app\common\repositories\store\ExcelRepository;
use app\common\repositories\store\order\MerchantReconciliationRepository;
use app\common\repositories\store\order\StoreOrderRepository;
use crmeb\exceptions\UploadException;
use crmeb\jobs\BatchDeliveryJob;
use crmeb\jobs\SendSmsJob;
use crmeb\jobs\SendTemplateMessageJob;
use think\App;
use crmeb\basic\BaseController;
use app\common\repositories\store\order\StoreOrderRepository as repository;
use think\facade\Queue;
//foxpur二开
use app\common\model\store\order\FoxpurOrder;
use app\common\model\store\order\StoreOrderStatus;
use app\common\model\system\admin\Admin;
use app\common\model\system\merchant\Merchant;
use app\common\model\store\product\ProductAttrValue;
use app\common\model\store\order\StoreRefundOrder;

class Order extends BaseController
{
    protected $repository;

    /**
     * Product constructor.
     * @param App $app
     * @param repository $repository
     */
    public function __construct(App $app, repository $repository)
    {
        parent::__construct($app);
        $this->repository = $repository;
    }


    public function title()
    {
        $where = $this->request->params(['status', 'date', 'order_sn', 'username', 'order_type', 'keywords', 'order_id', 'activity_type']);
        $where['mer_id'] = $this->request->merId();
        return app('json')->success($this->repository->getStat($where, $where['status']));
    }
    /**
     * 订单列表
     * @return mixed
     * @author Qinii
     */
    public function lst()
    {
        [$page, $limit] = $this->getPage();
        $where = $this->request->params(['status', 'date', 'order_sn', 'username', 'order_type', 'keywords', 'order_id', 'activity_type', 'group_order_sn', 'store_name']);
        $where['mer_id'] = $this->request->merId();
        return app('json')->success($this->repository->merchantGetList($where, $page, $limit));
    }

    public function takeTitle()
    {
        $where = $this->request->params(['date', 'order_sn', 'username', 'keywords']);
        $where['take_order'] = 1;
        $where['status'] = -1;
        $where['verify_date'] = $where['date'];
        unset($where['date']);
        $where['mer_id'] = $this->request->merId();
        return app('json')->success($this->repository->getStat($where, ''));
    }

    /**
     * TODO 自提订单列表
     * @return mixed
     * @author Qinii
     * @day 2020-08-17
     */
    public function takeLst()
    {
        [$page, $limit] = $this->getPage();
        $where = $this->request->params(['date', 'order_sn', 'username', 'keywords']);
        $where['take_order'] = 1;
        $where['status'] = -1;
        $where['verify_date'] = $where['date'];
        unset($where['date']);
        $where['mer_id'] = $this->request->merId();
        return app('json')->success($this->repository->merchantGetList($where, $page, $limit));
    }

    /**
     *  订单头部统计
     * @return mixed
     * @author Qinii
     */
    public function chart()
    {
        return app('json')->success($this->repository->OrderTitleNumber($this->request->merId(), null));
    }

    /**
     * TODO 自提订单头部统计
     * @return mixed
     * @author Qinii
     * @day 2020-08-17
     */
    public function takeChart()
    {
        return app('json')->success($this->repository->OrderTitleNumber($this->request->merId(), 1));
    }


    /**
     * TODO 订单类型
     * @return mixed
     * @author Qinii
     * @day 2020-08-15
     */
    public function orderType()
    {
        $where['mer_id'] = $this->request->merId();
        return app('json')->success($this->repository->orderType($where));
    }

    /**
     * @param $id
     * @return mixed
     * @author Qinii
     */
    public function deliveryForm($id)
    {
        $data = $this->repository->getWhere(['order_id' => $id, 'mer_id' => $this->request->merId(), 'is_del' => 0]);
        if (!$data) return app('json')->fail('数据不存在');
        if (!$data['paid']) return app('json')->fail('订单未支付');
        if (!in_array($data['status'], [0, 1])) return app('json')->fail('订单状态错误');
        return app('json')->success(formToData($this->repository->sendProductForm($id, $data)));
    }

    /**
     * TODO 发货
     * @param $id
     * @return mixed
     * @author Qinii
     */
    public function delivery($id)
    {
        $type = $this->request->param('delivery_type');
        if (!$this->repository->merDeliveryExists($id, $this->request->merId()))
            return app('json')->fail('订单信息或状态错误');
        if ($type == 4) {
            if (!systemConfig('crmeb_serve_dump')) return app('json')->fail('电子面单功能未开启');
            $params = $this->request->params([
                'delivery_name',
                'from_name',
                'from_tel',
                'from_addr',
                'temp_id',
            ]);
            $this->repository->dump($id, $this->request->merId(), $params);
        } else {
            $data  = $this->request->params([
                'delivery_type',
                'delivery_name',
                'delivery_id',
            ]);
            if (preg_match('/([\x81-\xfe][\x40-\xfe])/', $data['delivery_id']))
                return app('json')->fail('请输入正确的单号/电话');
            $this->repository->delivery($id, $data);
        }

        return app('json')->success('发货成功');
    }

    /**
     * TODO
     * @return \think\response\Json
     * @author Qinii
     * @day 7/26/21
     */
    public function batchDelivery()
    {
        $params = $this->request->params([
            'temp_id',
            'order_id',
            'from_tel',
            'from_addr',
            'from_name',
            'delivery_id',
            'delivery_type',
            'delivery_name',

        ]);
        if (!in_array($params['delivery_type'], [2, 3, 4]))  return app('json')->fail('发货类型错误');
        if (!$params['order_id'])  return app('json')->fail('需要订单ID');
        $data = [
            'mer_id' => $this->request->merId(),
            'data' => $params
        ];
        if ($params['delivery_type'] == 4 && !systemConfig('crmeb_serve_dump'))
            return app('json')->fail('电子面单功能未开启');
        //        $this->repository->batchDelivery($data['mer_id'],$data['data']);
        Queue::push(BatchDeliveryJob::class, $data);
        return app('json')->success('开始批量发货');
    }

    /**
     * TODO 改价form
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-11
     */
    public function updateForm($id)
    {
        if (!$this->repository->merStatusExists($id, $this->request->merId()))
            return app('json')->fail('订单信息或状态错误');
        return app('json')->success(formToData($this->repository->form($id)));
    }

    /**
     * TODO 改价
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-11
     */
    public function update($id)
    {
        $data = $this->request->params(['total_price', 'pay_postage']);
        if ($data['total_price'] < 0 || $data['pay_postage'] < 0)
            return app('json')->fail('金额不可未负数');
        if (!$this->repository->merStatusExists($id, $this->request->merId()))
            return app('json')->fail('订单信息或状态错误');
        $this->repository->eidt($id, $data);
        return app('json')->success('修改成功');
    }

    /**
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-11
     */
    public function detail($id)
    {
        $data = $this->repository->getOne($id, $this->request->merId());
        if (!$data) return app('json')->fail('数据不存在');
        return app('json')->success($data);
    }

    /**
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-11
     */
    public function status($id)
    {
        [$page, $limit] = $this->getPage();
        if (!$this->repository->getOne($id, $this->request->merId()))
            return app('json')->fail('数据不存在');
        return app('json')->success($this->repository->getOrderStatus($id, $page, $limit));
    }

    /**
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-11
     */
    public function remarkForm($id)
    {
        return app('json')->success(formToData($this->repository->remarkForm($id)));
    }

    /**
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-11
     */
    public function remark($id)
    {
        if (!$this->repository->getOne($id, $this->request->merId()))
            return app('json')->fail('数据不存在');
        $data = $this->request->params(['remark']);
        $this->repository->update($id, $data);

        return app('json')->success('备注成功');
    }

    /**
     * 核销
     * @param $code
     * @author xaboy
     * @day 2020/8/15
     */
    public function verify($code)
    {
        $this->repository->verifyOrder($code, $this->request->merId(), 0);
        return app('json')->success('订单核销成功');
    }

    /**
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-11
     */
    public function delete($id)
    {
        if (!$this->repository->userDelExists($id, $this->request->merId()))
            return app('json')->fail('订单信息或状态错误');
        $this->repository->merDelete($id);
        return app('json')->success('删除成功');
    }


    /**
     * TODO 快递查询
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-06-25
     */
    public function express($id)
    {
        return app('json')->success($this->repository->express($id, $this->request->merId()));
    }

    /**
     * TODO
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-07-30
     */
    public function reList($id)
    {
        [$page, $limit] = $this->getPage();
        $make = app()->make(MerchantReconciliationRepository::class);
        if (!$make->getWhereCount(['mer_id' => $this->request->merId(), 'reconciliation_id' => $id]))
            return app('json')->fail('数据不存在');
        $where = ['reconciliation_id' => $id, 'type' => 0];
        return app('json')->success($this->repository->reconList($where, $page, $limit));
    }

    /**
     * TODO 导出文件
     * @author Qinii
     * @day 2020-07-30
     */
    public function excel()
    {
        $where = $this->request->params(['status', 'date', 'order_sn', 'order_type', 'username', 'keywords', 'take_order']);
        if ($where['take_order']) {
            $where['status'] = -1;
            $where['verify_date'] = $where['date'];
            unset($where['date']);
            unset($where['order_type']);
        }
        $where['mer_id'] = $this->request->merId();
        app()->make(ExcelRepository::class)->create($where, $this->request->adminId(), 'order', $this->request->merId());
        return app('json')->success('开始导出数据');
    }

    /**
     * TODO 打印小票
     * @param $id
     * @return mixed
     * @author Qinii
     * @day 2020-07-30
     */
    public function printer($id)
    {
        $merId = $this->request->merId();
        $this->repository->checkPrinterConfig($merId);
        if (!$this->repository->getOne($id, $merId))
            return app('json')->fail('数据不存在');
        $this->repository->printer($id, $merId);
        return app('json')->success('打印成功');
    }

    /**
     * TODO 导出发货单
     * @return \think\response\Json
     * @author Qinii
     * @day 3/13/21
     */
    public function deliveryExport()
    {
        $where = $this->request->params(['username', 'date', 'activity_type', 'order_type', 'username', 'keywords', 'id']);
        $where['mer_id'] = $this->request->merId();
        $where['status'] = 0;
        $where['paid'] = 1;
        $make = app()->make(StoreOrderRepository::class);
        if (is_array($where['id'])) $where['order_ids'] = $where['id'];
        $count = $make->search($where)->count();
        if (!$count) app('json')->fail('没有可导出数据');
        app()->make(ExcelRepository::class)->create($where, $this->request->adminId(), 'delivery', $this->request->merId());
        return app('json')->success('开始导出数据');
    }


    //订单详细
    public function foxpur_detail($id)
    {
        $data = $this->repository->getOne($id,$this->request->merId());
        if(!$data) return app('json')->fail('数据不存在');
        $adminUid = $this->request->params(['adminFoxInfo']);
        $data['adminUid']=Admin::where('account',$adminUid['adminFoxInfo'])->value('admin_id');
        $data['foxpur_order_link'] = Merchant::where('mer_id',$data['mer_id'])->value('foxpur_order_link');  //获取商家链接
        $data['foxpur']=FoxpurOrder::where('order_id',$data['order_id'])->find();
        $data['refund']=StoreRefundOrder::where('order_id',$data['order_id'])->order('refund_order_id DESC')->find();
        return app('json')->success($data);
    }

    //订单详细购物车详细接口
    public function foxpur_cart($id){
        $page = 1;
        $limit = 1;
        $where = $this->request->params(['status', 'date', 'order_sn','username','order_type','keywords','order_id','activity_type','cart_id']);
        $where['order_id'] = $id;
        $where['mer_id'] = $this->request->merId();
//        $where[''] = $this->request->merId();
        $res = $this->repository->merchantGetList($where, $page, $limit);
        $list = $res['list'];
        return app('json')->success($list[0]);
    }

    public function foxpur_url($unique){
        //获取属性代购链接
        $foxurl=ProductAttrValue::where('unique', $unique)->value('foxurl');
        $data['foxurl']=$foxurl;
        return app('json')->success($data);
    }

//foxpur保存备注订单
    public function foxpur_save(){
        $where = $this->request->params(['order_status', 'order_id','merorder_sn','adminFoxInfo','mer_id','foxremarks','product_unique']);




        $foxpur_order_link=Merchant::where('mer_id',$where['mer_id'])->value('foxpur_order_link');  //获取商家链接
        $res=FoxpurOrder::where('order_id',$where['order_id'])->find();     //获取foxpur管理订单状态信息
        $adminUid=Admin::where('account',$where['adminFoxInfo'])->value('admin_id');
        if (!$adminUid)
            return app('json')->fail('请用高级管理员身份登录！');
        $where['orderer_uid']=$res['orderer_uid'];
        $where['logistics_uid']=$res['logistics_uid'];

        if($res['order_status']!=$where['order_status']){
            switch ($where['order_status'])
            {
                case 1:
                    $order_status_text='未发货（处理中）';
                    break;
                case 2:
                    $order_status_text='缺货';
                    break;
                case 3:
                    $order_status_text='无货源';
                    Queue::push(SendSmsJob::class, ['tempId' => 'FOXPUR_OUT_OF_STOCK_NOTICE', 'id' => $where['order_id']]);
                    queue::push(SendTemplateMessageJob::class,['tempCode' => 'FOXPUR_OUT_OF_STOCK_NOTICE','id' => $where['order_id']]);
                    break;
                case 4:
                    $order_status_text='已转账';
                    $where['orderer_uid']=$adminUid;
                    break;
                case 5:
                    $order_status_text='已下单';
                    $where['orderer_uid']=$adminUid;
                    break;
                case 6:
                    $order_status_text='物流已更新';
                    $where['logistics_uid']=$adminUid;
                    break;
                case 7:
                    $order_status_text='申请售后';
                    break;
                case 8:
                    $order_status_text='售后处理中';
                    break;
                case 9:
                    $order_status_text='同意退换';
                    break;
                case 10:
                    $order_status_text='拒绝售后';
                    break;
                case 11:
                    $order_status_text='买家提供退回信息';
                    break;
                case 12:
                    $order_status_text='退货已提交卖家';
                    break;
                case 13:
                    $order_status_text='换货已提交卖家';
                    break;
                case 14:
                    $order_status_text='卖家已退款';
                    break;
                case 15:
                    $order_status_text='可优先退款';
                    break;
                case 16:
                    $order_status_text='卖家未退款';
                    break;
                case 17:
                    $order_status_text='可退款';
                    break;
                case 18:
                    $order_status_text='已退款';
                    break;
                case 19:
                    $order_status_text='售后完成';
                    break;
                case 20:
                    $order_status_text='换货已下单';
                    break;
                default:
                    $order_status_text='';
                    break;
            }
        }else{$order_status_text='';}



        if($res){
            FoxpurOrder::where('order_id',$where['order_id'])->update(['order_status'=>$where['order_status'],'merorder_sn'=>$where['merorder_sn'],'orderer_uid'=>$where['orderer_uid'],'logistics_uid'=>$where['logistics_uid'],'product_unique'=>$where['product_unique']]);
        }else{
            FoxpurOrder::create($where);
        }



        if($where['merorder_sn']!=$res['merorder_sn']){
            $foxpur_order_link_text="<a href='".$foxpur_order_link.$where['merorder_sn']."' target='__blank'>".$where['merorder_sn']."</a>";
        }else{
            $foxpur_order_link_text='';
        }

        $message=$where['adminFoxInfo']."更新 ".$order_status_text.$where['foxremarks'].$foxpur_order_link_text;

        $data=['order_id'=>$where['order_id'],'change_message'=>$message,'change_time'=>date("Y-m-d H:i:s")];
        StoreOrderStatus::create($data);
        return app('json')->success('修改成功');

    }

    /**
     * @param $id
     * @return foxpur买家备注提示
     * @author Qinii
     * @day 2020-06-11
     */
    public function foxpurremarkForm($id)
    {
        $adminid=$this->request->params(['adminFoxInfo']);
        return app('json')->success(formToData($this->repository->foxpurremarkForm($id,$adminid['adminFoxInfo'])));
    }

    /**
     * @param $id
     * @return foxpu买家提示备注
     * @author Qinii
     * @day 2020-06-11
     */
    public function foxpurremark($id,$adminid)
    {
        if (!$this->repository->getOne($id, $this->request->merId()))
            return app('json')->fail('数据不存在');
        if (!$adminid)
            return app('json')->fail('请用高级管理员身份登录！');

        $data = $this->request->params(['remark']);
        //foxpur买家备注
        $this->repository->update($id, $data);
        $foxdata=['order_id'=>$id,'change_type'=>'','change_message'=>$adminid.'买家可见备注：'.implode($data),'change_time'=>date("Y-m-d H:i:s")];
        StoreOrderStatus::create($foxdata);
        return app('json')->success('备注成功！');

    }

}
