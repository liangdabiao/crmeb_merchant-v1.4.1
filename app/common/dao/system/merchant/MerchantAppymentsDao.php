<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\common\dao\system\merchant;

use app\common\dao\BaseDao;
use app\common\model\system\merchant\MerchantApplyments;

class MerchantAppymentsDao extends BaseDao
{
    protected function getModel(): string
    {
        return MerchantApplyments::class;
    }

    public function search(array $where)
    {
        $query = $this->getModel()::getDB()
            ->when(isset($where['mer_id']) && $where['mer_id'] !== '', function ($query) use ($where) {
                $query->where('mer_id', $where['mer_id']);
            })
            ->when(isset($where['uid']) && $where['uid'] !== '', function ($query) use ($where) {
                $query->where('uid', $where['uid']);
            })
            ->when(isset($where['status']) && $where['status'] !== '', function ($query) use ($where) {
                $query->where('status', (int)$where['status']);
            })
            ->when(isset($where['mer_applyments_id']) && $where['mer_applyments_id'] !== '', function ($query) use ($where) {
                $query->where('mer_applyments_id', $where['mer_applyments_id']);
            })
            ->when(isset($where['date']) && $where['date'] !== '', function ($query) use ($where) {
                getModelTime($query, $where['date']);
            })
            ->where('is_del', 0);

        return $query;
    }


    /**
     * TODO 经营者/法人证件类型
     * @param $key
     * @return array|mixed
     * @author Qinii
     * @day 6/22/21
     */
    public function getIdDocType($key)
    {
        $data = [
            1 => MerchantApplyments::IDCARD,
            2 => MerchantApplyments::PASSPORT,
            3 => MerchantApplyments::HONGKONG,
            4 => MerchantApplyments::MACAO,
            5 => MerchantApplyments::TAIWAN,
        ];
        if($key) return $data[$key];
        return $data;
    }
}
