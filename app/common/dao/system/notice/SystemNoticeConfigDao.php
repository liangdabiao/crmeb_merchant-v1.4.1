<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\common\dao\system\notice;


use app\common\dao\BaseDao;
use app\common\model\system\notice\SystemNoticeConfig;

class SystemNoticeConfigDao extends BaseDao
{

    protected function getModel(): string
    {
        return SystemNoticeConfig::class;
    }


    public function getNoticeStatusByKey(string $key, string $field)
    {
        $value = $this->getModel()::getDb()->where('notice_key',$key)->value($field);
        return $value == 1  ? true  : false;
    }

}
