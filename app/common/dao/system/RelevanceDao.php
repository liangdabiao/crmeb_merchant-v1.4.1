<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\common\dao\system;

use app\common\dao\BaseDao;
use app\common\model\system\Relevance;
use app\common\repositories\system\RelevanceRepository;

class RelevanceDao extends BaseDao
{

    protected function getModel(): string
    {
        return Relevance::class;
    }

    public function clear(int $id, string $type, string $field)
    {
        return $this->getModel()::getDb()->where($field, $id)->where('type', $type)->delete();
    }


    public function joinUser($uid)
    {
        $query = Relevance::hasWhere('community',function($query) use($uid){
            $query->where('status',1)->where('is_show',1)->where('is_del',0);
        });

        $query->where('left_id',$uid)->where('type',RelevanceRepository::TYPE_COMMUNITY_START);
        return $query;
    }
}
