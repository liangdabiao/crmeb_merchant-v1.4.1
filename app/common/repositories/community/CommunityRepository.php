<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace app\common\repositories\community;

use app\common\dao\community\CommunityDao;
use app\common\repositories\BaseRepository;
use app\common\repositories\store\order\StoreOrderProductRepository;
use app\common\repositories\store\product\SpuRepository;
use app\common\repositories\system\RelevanceRepository;
use app\common\repositories\user\UserBrokerageRepository;
use app\common\repositories\user\UserRepository;
use crmeb\services\CacheService;
use FormBuilder\Factory\Elm;
use think\exception\ValidateException;
use think\facade\Db;
use think\facade\Route;

class CommunityRepository extends BaseRepository
{
    /**
     * @var CommunityDao
     */
    protected $dao;

    const IS_SHOW_WHERE = [
        'is_show' => 1,
        'status'  => 1,
        'is_del'  => 0,
    ];

    /**
     * CommunityRepository constructor.
     * @param CommunityDao $dao
     */
    public function __construct(CommunityDao $dao)
    {
        $this->dao = $dao;
    }

    public function getList(array $where, int $page, int $limit)
    {
        $where['is_del'] = 0;
        $query = $this->dao->search($where)->with([
            'author' => function($query) {
                $query->field('uid,real_name,status,avatar,nickname,count_start');
            },
            'topic' => function($query) {
                $query->where('status', 1)->where('is_del',0);
                $query->field('topic_id,topic_name,status,category_id,pic,is_del');
            },
            'category'
        ]);
        $count = $query->count();
        $list = $query->page($page, $limit)->select();

        return compact('count','list');
    }

    public function getApiList(array $where, int $page, int $limit, $userInfo)
    {
        $where['is_del'] = 0;
        $query = $this->dao->getSearch($where)->order('start DESC,create_time DESC,community_id DESC');
        $query->with([
            'author' => function($query) use($userInfo){
                $query->field('uid,real_name,status,avatar,nickname,count_start');
            },
            'is_start' => function($query) use ($userInfo) {
                $query->where('left_id',$userInfo->uid ?? null);
            },
            'topic' => function($query) {
                $query->where('status', 1)->where('is_del',0);
                $query->field('topic_id,topic_name,status,category_id,pic,is_del');
            },
            'relevance'  => [
                'spu' => function($query) {
                    $query->field('spu_id,store_name,image,price,product_type,activity_id,product_id');
                }
            ],
            'is_fans' => function($query) use($userInfo){
                $query->where('left_id',$userInfo->uid??  0);
            }
        ]);
        $count = $query->count();
        $list = $query->page($page, $limit)->setOption('field',[])
            ->field('community_id,title,image,topic_id,count_start,count_reply,start,create_time,uid,status,is_show,content')
            ->select()->append(['time']);

        return compact('count','list');
    }

    /**
     * TODO 后台详情
     * @param int $id
     * @return array|\think\Model|null
     * @author Qinii
     * @day 10/28/21
     */
    public function detail(int $id)
    {
        return $this->dao->getSearch([$this->dao->getPk() => $id, 'is_del' => 0])->with([
            'author' => function($query) {
                $query->field('uid,real_name,status,avatar,nickname,count_start');
            },
            'topic',
            'category',
            'relevance.spu'
        ])->find();
    }

    /**
     * TODO 移动端详情展示
     * @param int $id
     * @param $user
     * @return array|\think\Model|null
     * @author Qinii
     * @day 10/27/21
     */
    public function show(int $id, $user)
    {
        return CacheService::create(0,CacheService::TAG_COMMUNITY)->remember($id, function ()use($id, $user){
            $where = self::IS_SHOW_WHERE;
            $is_author = $is_fans =0;
            if ($user && $this->dao->uidExists($id, $user->uid)) {
                $where = ['is_del' => 0];
                $is_author = 1;
            }
            $where[$this->dao->getPk()] = $id;
            $data = $this->dao->getSearch($where)
                ->with([
                    'author' => function ($query) {
                        $query->field('uid,real_name,status,avatar,nickname,count_start,member_level');
                        if (systemConfig('member_status')) $query->with(['member' => function ($query) {
                            $query->field('brokerage_icon,brokerage_level');
                        }]);
                    },
                    'relevance' => [
                        'spu' => function ($query) {
                            $query->field('spu_id,store_name,image,price,product_type,activity_id,product_id');
                        }
                    ],
                    'topic' => function ($query) {
                        $query->where('status', 1)->where('is_del', 0);
                        $query->field('topic_id,topic_name,status,category_id,pic,is_del');
                    },
                    'is_start' => function ($query) use ($user) {
                        $query->where('left_id', $user->uid ?? '');
                    },
                ])->hidden(['is_del'])->find();

            if (!$data) throw new ValidateException('内容不存在，可能已被删除了哦～');

            if ($user && !$is_author) $is_fans = app()->make(RelevanceRepository::class)->getWhereCount([
                'left_id' => $user->uid,
                'right_id' => $data['uid'],
                'type' => RelevanceRepository::TYPE_COMMUNITY_FANS,
            ]);

            $data['is_author'] = $is_author;
            $data['is_fans'] = $is_fans;
            return $data;
        },60);

    }

    public function getSpuByOrder($id, $uid)
    {
        $where = app()->make(StoreOrderProductRepository::class)->selectWhere(['order_id' => $id]);
        if (!$where) throw new  ValidateException('商品已下架');

        $make = app()->make(SpuRepository::class);
        foreach ($where as $item) {
            switch ($item['product_type']){
                case 0:
                    $id = $item['product_id'];
                   // nobreak;
                case 1:
                    $id = $item['product_id'];
                    break;
                case 2:
                    $id = $item['activity_id'];
                    break;
                case 3:
                    $id = $item['cart_info']['productAssistSet']['product_assist_id'];
                    break;
                case 4:
                    $id = $item['cart_info']['product']['productGroup']['product_group_id'];
                    break;
            }
            $data[] = $make->getSpuData($id, $item['product_type'],0);
        }
        return $data;
    }

    /**
     * TODO 创建
     * @param array $data
     * @author Qinii
     * @day 10/29/21
     */
    public function create(array $data)
    {
        event('community.create.before',compact('data'));
        if ($data['topic_id']) {
            $getTopic  =  app()->make(CommunityTopicRepository::class)->get($data['topic_id']);
            if (!$getTopic || !$getTopic->status) throw new ValidateException('话题不存在或已关闭');
            $data['category_id'] = $getTopic->category_id;
        }
        return Db::transaction(function () use($data) {
            $community = $this->dao->create($data);
            if ($data['spu_id'])$this->joinProduct($community->community_id,$data['spu_id']);
            event('community.create',compact('community'));
            return $community->community_id;
        });
    }

    /**
     * TODO 编辑
     * @param int $id
     * @param array $data
     * @author Qinii
     * @day 10/29/21
     */
    public function edit(int $id, array $data)
    {
        event('community.update.before',compact('id','data'));
        if ($data['topic_id']) {
            $getTopic  =  app()->make(CommunityTopicRepository::class)->get($data['topic_id']);

            if (!$getTopic || !$getTopic->status) throw new ValidateException('话题不存在或已关闭');
            $data['category_id'] = $getTopic->category_id;
        }

        Db::transaction(function () use($id, $data) {
            $spuId = $data['spu_id'];
            unset($data['spu_id']);
            $community = $this->dao->update($id, $data);
            if ($spuId) $this->joinProduct($id, $spuId);
            $this->clearCache($id);
            event('community.update.before',compact('id','community'));
        });
    }

    public function joinProduct($id, array $data)
    {
        $make = app()->make(RelevanceRepository::class);
        $data = array_unique($data);
        $res = [];
        foreach ($data as $value) {
            if ($value) {
                $res[] = [
                    'left_id' => $id,
                    'right_id' => $value,
                    'type' => RelevanceRepository::TYPE_COMMUNITY_PRODUCT
                ];
            }
        }
        $make->clear($id,RelevanceRepository::TYPE_COMMUNITY_PRODUCT,'left_id');
        if($res) $make->insertAll($res);
    }

    /**
     * TODO 获取某用户信息
     * @param int $uid
     * @param null $self
     * @return mixed
     * @author Qinii
     * @day 10/29/21
     */
    public function getUserInfo(int $uid, $self = null)
    {
        $relevanceRepository = app()->make(RelevanceRepository::class);
        $data['focus'] = $relevanceRepository->getFieldCount('left_id', $uid,RelevanceRepository::TYPE_COMMUNITY_FANS);


        $is_start = $is_self = false;
        if ($self && $self->uid == $uid) {
            $user = $self;
            $is_self = true;
        } else {
            $user = app()->make(UserRepository::class)->get($uid);
            $is_start = $relevanceRepository->checkHas($self->uid, $uid, RelevanceRepository::TYPE_COMMUNITY_FANS) > 0;
        }
        $data['start'] = $user->count_start;
        $data['uid']   = $user->uid;
        $data['avatar'] = $user->avatar;
        $data['nickname'] = $user->nickname;
        $data['is_start'] = $is_start;
        $data['member_icon'] = systemConfig('member_status') ? ($user->member->brokerage_icon ?? '')  : '';
        $data['is_self'] = $is_self;
        $data['fans'] = $user->count_fans;

        return $data;
    }

    public function setFocus(int $id, int  $uid,int $status)
    {
        $make = app()->make(RelevanceRepository::class);
        $check  = $make->checkHas($uid, $id, RelevanceRepository::TYPE_COMMUNITY_FANS);
        if ($status) {
            if ($check)  throw new ValidateException('您已经关注过他了～');
            $make->create($uid, $id,RelevanceRepository::TYPE_COMMUNITY_FANS,true);
            app()->make(UserRepository::class)->incField($id, 'count_fans', 1);
        } else {
            if (!$check) throw new ValidateException('您还未关注他哦～');
            $make->destory($uid, $id,RelevanceRepository::TYPE_COMMUNITY_FANS);
            app()->make(UserRepository::class)->decField($id, 'count_fans', 1);
        }
        return ;
    }

    public function form($id)
    {
        $form = Elm::createForm(Route::buildUrl('systemCommunityUpdate', ['id' => $id])->build());
        $data = $this->dao->get($id);
        if (!$data) throw new ValidateException('数据不存在');
        $formData = $data->toArray();

        return $form->setRule([
            Elm::rate('start', '排序星级')->max(5)
        ])->setTitle('编辑星级')->formData($formData);
    }

    public function showForm($id)
    {
        $form = Elm::createForm(Route::buildUrl('systemCommunityStatus', ['id' => $id])->build());
        $data = $this->dao->get($id);
        if (!$data) throw new ValidateException('数据不存在');

        $formData = $data->toArray();

        return $form->setRule([
            Elm::radio('status', '强制下架')->options([
                ['value' => -2, 'label' => '下架'], ['value' => 1, 'label' => '上架']])->control([
                ['value' => -2, 'rule' => [
                    Elm::textarea('refusal', '下架理由', '信息存在违规')->required()
                ]]
            ]),
        ])->setTitle('强制下架')->formData($formData);
    }

    public function setCommunityStart(int $id, $userInfo, int $status)
    {
        $make = app()->make(RelevanceRepository::class);
        $userRepository = app()->make(UserRepository::class);

        if ($status) {
            $res = $make->create($userInfo->uid, $id, RelevanceRepository::TYPE_COMMUNITY_START,true);
            if (!$res) throw new ValidateException('您已经点赞过了');

            $ret = $this->dao->get($id);
            $user = $userRepository->get($ret['uid']);
            $this->dao->incField($id,'count_start',1);
            $userRepository->incField((int)$user->uid,'count_start',1);
        }
        if (!$status) {
            if (!$make->checkHas($userInfo->uid, $id, RelevanceRepository::TYPE_COMMUNITY_START))
                throw new ValidateException('您还没赞过了');
            $make->destory($userInfo->uid, $id, RelevanceRepository::TYPE_COMMUNITY_START);

            $ret = $this->dao->get($id);
            $user = $userRepository->get($ret['uid']);
            $this->dao->decField($id,'count_start',1);
            app()->make(UserRepository::class)->decField((int)$user->uid, 'count_start',1);
        }
    }

    public function setStatus($id, $data)
    {
        $ret = $this->dao->get($id);
        event('community.status.before',compact('id','data'));
        Db::transaction(function () use($ret,$id, $data) {

            if ($data['status'] == 1) {
                $make = app()->make(UserBrokerageRepository::class);
                $make->incMemberValue($ret['uid'], 'member_community_num', $id);
            } else {
               $this->clearCache($id);
            }

            $this->dao->update($id, $data);
            event('community.status',compact('id'));
        });

    }

    public function clearCache($id)
    {
        $cacheKey = CacheService::create(0,CacheService::TAG_COMMUNITY)->cacheKey($id);
        CacheService::delete($cacheKey);
    }

    public function destory($id, $user = null)
    {
        event('community.delete.before',compact('id','user'));
        $this->dao->update($id,  ['is_del' =>  1]);
        $this->clearCache($id);
        event('community.delete',compact('id', 'user'));
    }

    public function getDataBySpu($spuId)
    {
        $where = array_merge(['spu_id' => $spuId], self::IS_SHOW_WHERE);
        return $this->dao->getSearch($where)
            ->field('community_id,title,image')
            ->limit(3)->select();
    }
}

