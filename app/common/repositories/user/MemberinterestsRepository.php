<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\common\repositories\user;


use app\common\dao\user\MemberInterestsDao;
use app\common\dao\user\UserBrokerageDao;
use app\common\repositories\BaseRepository;
use FormBuilder\Factory\Elm;
use think\exception\ValidateException;
use think\facade\Route;

/**
 * @mixin UserBrokerageDao
 */
class MemberinterestsRepository extends BaseRepository
{


    public function __construct(MemberInterestsDao $dao)
    {
        $this->dao = $dao;
    }

    public function getList(array $where, int $page, int $limit)
    {
        $query = $this->dao->getSearch($where);
        $count = $query->count();
        $list = $query->page($page, $limit)->select();
        return compact('count','list');
    }

    public function form(?int $id = null)
    {
        $formData = [];
        if ($id) {
            $form = Elm::createForm(Route::buildUrl('systemUserMemberInterestsUpdate', ['id' => $id])->build());
            $data = $this->dao->get($id);
            if (!$data) throw new ValidateException('数据不存在');
            $formData = $data->toArray();

        } else {
            $form = Elm::createForm(Route::buildUrl('systemUserMemberInterestsCreate')->build());
        }

        $rules = [
            Elm::input('name', '权益名称')->required(),
            Elm::input('info', '权益简介')->required(),
            Elm::frameImage('pic', '图标', '/' . config('admin.admin_prefix') . '/setting/uploadPicture?field=pic&type=1')
                ->value($formData['pic'] ?? '')
                ->modal(['modal' => false])
                ->width('896px')
                ->height('480px'),
            Elm::select('brokerage_level', '会员级别')->options(function () {
                $options = app()->make(UserBrokerageRepository::class)->options(['type' => 1])->toArray();
                return $options;
            }),
        ];
        $form->setRule($rules);
        return $form->setTitle(is_null($id) ? '添加权益' : '编辑权益')->formData($formData);
    }

    public function getInterestsByLevel(int $level, int $type)
    {
        if (systemConfig('member_interests_status')) return [];

        $list = $this->dao->getSearch(['type' => $type])->select();
        foreach ($list as $item) {
            $item['status'] = 0;
            if ($item['brokerage_level'] <= $level) {
                $item['status'] = 1;
            }
        }
        return $list;
    }
}
