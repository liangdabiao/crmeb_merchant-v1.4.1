<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\common\repositories\system\merchant;


use app\common\dao\system\merchant\MerchantTypeDao;
use app\common\repositories\BaseRepository;
use think\facade\Db;
use think\facade\Route;

/**
 * @mixin MerchantTypeDao
 */
class MerchantTypeRepository extends BaseRepository
{
    public function __construct(MerchantTypeDao $dao)
    {
        $this->dao = $dao;
    }

    public function getList($page, $limit)
    {
        $query = $this->dao->search();
        $count = $query->count();
        $list = $query->page($page, $limit)->select();
        return compact('count', 'list');
    }

    public function getSelect()
    {
        $query = $this->search([])->field('mer_type_id,type_name');
        return $query->select()->toArray();
    }

    public function delete(int $id)
    {
        return Db::transaction(function () use ($id) {
            $this->dao->delete($id);
            app()->make(MerchantRepository::class)->clearTypeId($id);
        });
    }
}