<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\common\repositories\system;


use app\common\dao\system\CacheDao;
use app\common\repositories\BaseRepository;
use think\db\exception\DbException;
use think\exception\ValidateException;
use think\facade\Cache;

/**
 * Class CacheRepository
 * @package app\common\repositories\system
 * @author xaboy
 * @day 2020-04-24
 * @mixin CacheDao
 */
class CacheRepository extends BaseRepository
{
    /**
     * CacheRepository constructor.
     * @param CacheDao $dao
     */
    public function __construct(CacheDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * @param string $key
     * @param $result
     * @param int $expire_time
     * @throws DbException
     * @author xaboy
     * @day 2020-04-24
     */
    public function save(string $key, $result, int $expire_time = 0)
    {
        if (!$this->dao->fieldExists('key', $key)) {
            $this->dao->create(compact('key', 'result', 'expire_time'));
        } else {
            $this->dao->keyUpdate($key, compact('result', 'expire_time'));
        }
    }












    public function addres()
    {
        return [];
        $re = (Cache::get('AAAAAA'));
        //halt($re);
        unset($re['省市编码']);
        if (!$re) throw new ValidateException('无数据');
        $shen = [];
        $shi = [];
        $qu = [];
        foreach ($re as $key => $value) {
            $item = explode(',', $value);
            $cout = count($item);
            //省
            if ($cout == 2) {
                $shen[$item[1]] = [
                    'value' => $key,
                    'label' => $item[1],
                ];
            }
            //市
            if ($cout == 3) {
                if ($item[1] == '') {
                    $shen[$item[2]] = [
                        'value' => $key,
                        'label' => $item[2],
                    ];
                    $item[1] = $item[2];
                }
                $_v = [
                    'value' => $key,
                    'label' => $item[2]
                ];
                $shi[$item[1]][] = $_v;
            }
            //区
            if ($cout == 4) {
                $_v = [
                    'value' => $key,
                    'label' => $item[3]
                ];
                $qu[$item[2]][] = $_v;
            }
        }
        $data = [];
        foreach ($shen as $s => $c) {
            foreach ($shi as $i => $c_) {
                if ($c['label'] == $i) {
                    if ($c['label'] == $i) {
                        $san = [];
                        foreach ($c_ as $key => $value) {
                            if (isset($qu[$value['label']])) {
                                $value['children'] = $qu[$value['label']];
                            }
                            $san[] = $value;
                        }
                    }
                    $c['children'] = $san;
                }
            }
            $zls[$s] = $c;
        }
        $data = array_values($zls);
        file_put_contents('address.js', json_encode($data, JSON_UNESCAPED_UNICODE));
        //$this->save('applyments_addres',$data);
    }
}
