<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\common\repositories\system;


use app\common\dao\system\RelevanceDao;
use app\common\repositories\BaseRepository;
use think\exception\ValidateException;
use think\facade\Db;

class RelevanceRepository extends BaseRepository
{

    //文章关联商品
    const TYPE_COMMUNITY_PRODUCT  =  'community_product';
    //社区关注
    const TYPE_COMMUNITY_FANS  =  'fans';
    //社区文章点赞
    const TYPE_COMMUNITY_START  =  'community_start';
    //社区评论点赞
    const TYPE_COMMUNITY_REPLY_START  =  'community_reply_start';

    protected $dao;
    /**
     * RelevanceRepository constructor.
     * @param RelevanceDao $dao
     */
    public function __construct(RelevanceDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * TODO 添加
     * @param int $leftId
     * @param int $rightId
     * @param string $type
     * @param $check
     * @return bool
     * @author Qinii
     * @day 10/28/21
     */
    public function create(int $leftId, int $rightId, string $type, bool $check = false)
    {
        if ($check && $this->checkHas($leftId, $rightId, $type))  {
            return false;
        }

        $data = [
            'left_id' => $leftId,
            'right_id'=> $rightId,
            'type'    => $type,
        ];

        try{
            $this->dao->create($data);
            return true;
        } catch (\Exception  $exception) {
            throw new ValidateException('创建失败');
        }
    }

    /**
     * TODO 删除
     * @param int $leftId
     * @param string $type
     * @param int $rightId
     * @return bool
     * @author Qinii
     * @day 10/28/21
     */
    public function destory(int $leftId, int $rightId, string $type)
    {
        return $this->dao->getSearch([
            'left_id' => $leftId,
            'right_id'=> $rightId,
            'type'    => $type,
        ])->delete();
    }

    /**
     * TODO 检测是否存在
     * @param int $leftId
     * @param int $rightId
     * @param string $type
     * @return int
     * @author Qinii
     * @day 10/28/21
     */
    public function checkHas(int $leftId, int $rightId, string $type)
    {
        return $this->dao->getSearch([
            'left_id' => $leftId,
            'right_id'=> $rightId,
            'type'    => $type,
        ])->count();
    }

    /**
     * TODO 根据左键批量删除
     * @param int $leftId
     * @param $type
     * @return bool
     * @author Qinii
     * @day 10/28/21
     */
    public function batchDelete(int $leftId, $type)
    {
        return $this->dao->getSearch([
            'left_id' => $leftId,
            'type'    => $type,
        ])->delete();
    }

    /**
     * TODO 关注我的人
     * @param int $uid
     * @return \think\Collection
     * @author Qinii
     * @day 10/28/21
     */
    public function getUserFans(int $uid, int $page, int $limit)
    {
        $query = $this->dao->getSearch([
            'right_id' => $uid,
            'type'    => self::TYPE_COMMUNITY_FANS,
        ])->with([
            'fans' => function($query) {
                $query->field('uid,avatar,nickname,count_fans');
            }
        ]);
        $count = $query->count();
        $list = $query->page($page, $limit)->select()->append(['is_start']);

        return compact('count','list');
    }

    /**
     * TODO 我关注的人
     * @param $uid
     * @return \think\Collection
     * @author Qinii
     * @day 10/28/21
     */
    public function getUserFocus(int $uid, int $page, int $limit)
    {
        $query = $this->dao->getSearch([
            'left_id' => $uid,
            'type'    => self::TYPE_COMMUNITY_FANS,
        ])->with([
            'focus' => function($query) {
                $query->field('uid,avatar,nickname,count_fans');
            }
        ]);
        $count = $query->count();
        $list = $query->page($page, $limit)->select();
        return compact('count','list');
    }


    /**
     * TODO 我点赞过的文章
     * @param int $uid
     * @return \think\Collection
     * @author Qinii
     * @day 10/28/21
     */
    public function getUserStartCommunity(int $uid, int $page, int $limit)
    {
        $query = $this->dao->joinUser($uid)->with([
            'community'=> function($query) {
                $query->with(['author'=> function($query) {
                    $query->field('uid,avatar,nickname');
                }]);
            },
        ]);
        $count = $query->count();
        $list = $query->page($page, $limit)->select()->each(function ($item){
            $item['time'] = date('m月d日', strtotime($item['create_time']));
            return $item;
        });

        return compact('count','list');
    }

    public function getFieldCount(string $field, int $value, string $type)
    {
        return $this->dao->getSearch([$field => $value, 'type'    => $type,])->count();
    }
}
