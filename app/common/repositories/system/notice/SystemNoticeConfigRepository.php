<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace app\common\repositories\system\notice;


use app\common\dao\system\notice\SystemNoticeConfigDao;
use app\common\repositories\BaseRepository;
use FormBuilder\Factory\Elm;
use think\exception\ValidateException;
use think\facade\Route;

/**
 * Class SystemNoticeConfigRepository
 * @package app\common\repositories\system\notice
 * @author xaboy
 * @day 2020/11/6
 * @mixin SystemNoticeConfigDao
 */
class SystemNoticeConfigRepository extends BaseRepository
{
    public function __construct(SystemNoticeConfigDao $dao)
    {
        $this->dao = $dao;
    }

    public function getList(array $where, $page, $limit)
    {
        $query = $this->dao->getSearch($where);
        $count = $query->count();
        $list = $query->page($page, $limit)->order('create_time DESC')->select();
        return compact('count', 'list');
    }

    public function form(?int $id)
    {
        $formData = [];
        if ($id) {
            $data = $this->dao->get($id);
            if (!$data) throw new ValidateException('数据不存在');
             $formData  = $data->toArray();
            $form = Elm::createForm(Route::buildUrl('systemNoticeConfigUpdate', ['id' => $id])->build());
        }  else {
            $form = Elm::createForm(Route::buildUrl('systemNoticeConfigCreate')->build());
        }

        $form->setRule([
            Elm::input('notice_title', '通知名称')->required(),
            Elm::input('notice_info', '通知说明')->required(),
            Elm::input('notice_key', '通知KEY')->required(),
            Elm::radio('notice_sys', '站内消息', -1)->options([
                ['value' => 0, 'label' => '关闭'],
                ['value' => 1, 'label' => '开启'],
                ['value' => -1, 'label' => '无'],
            ])->required(),
            Elm::radio('notice_wechat', '公众号模板消息', -1)->options([
                ['value' => 0, 'label' => '关闭'],
                ['value' => 1, 'label' => '开启'],
                ['value' => -1, 'label' => '无'],
            ])->required(),
            Elm::radio('notice_routine', '小程序订阅消息', -1)->options([
                ['value' => 0, 'label' => '关闭'],
                ['value' => 1, 'label' => '开启'],
                ['value' => -1, 'label' => '无'],
            ])->required(),
            Elm::radio('notice_sms', '短信消息', -1)->options([
                ['value' => 0, 'label' => '关闭'],
                ['value' => 1, 'label' => '开启'],
                ['value' => -1, 'label' => '无'],
            ])->required(),
            Elm::radio('type', '通知类型', 0)->options([
                ['value' => 0, 'label' => '用户'],
                ['value' => 1, 'label' => '商户'],
            ])->required(),
        ]);

        return $form->setTitle(is_null($id) ? '添加通知' : '编辑通知')->formData($formData);
    }

    public function swithStatus($id, $filed, $status)
    {
        $data = $this->dao->get($id);
        if ($data[$filed] == -1) throw  new ValidateException('该消息无此通知类型');
        $data->$filed = $status;
        $data->save();
    }

    /**
     * TODO
     * @param $key
     * @return bool
     * @author Qinii
     * @day 11/19/21
     */
    public function getNoticeSys($key)
    {
        return $this->dao->getNoticeStatusByKey($key, 'notice_sys');
    }

    /**
     * TODO
     * @param $key
     * @return bool
     * @author Qinii
     * @day 11/19/21
     */
    public function getNoticeSms($key)
    {
        return $this->dao->getNoticeStatusByKey($key, 'notice_sms');
    }

    /**
     * TODO
     * @param $key
     * @return bool
     * @author Qinii
     * @day 11/19/21
     */
    public function getNoticeWechat($key)
    {
        return $this->dao->getNoticeStatusByKey($key, 'notice_wechat');
    }

    /**
     * TODO
     * @param $key
     * @return bool
     * @author Qinii
     * @day 11/19/21
     */
    public function getNoticeRoutine($key)
    {
        return $this->dao->getNoticeStatusByKey($key, 'notice_routine');
    }

}
