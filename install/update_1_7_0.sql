INSERT INTO `eb_system_config` (`config_classify_id`, `config_name`, `config_key`, `config_type`, `config_rule`, `required`, `info`, `sort`, `user_type`, `status`, `create_time`) VALUES (28, '评论开启', 'sys_reply_status', 'radio', '0:关闭\n1:开启', 0, '关闭后移动端不展示评论', 0, 0, 1, '2021-10-19 14:38:49');
INSERT INTO `eb_system_config` (`config_classify_id`, `config_name`, `config_key`, `config_type`, `config_rule`, `required`, `info`, `sort`, `user_type`, `status`, `create_time`) VALUES (61, '社区开启', 'community_status', 'radio', '0:关闭\n1:开启', 0, '', 0, 0, 1, '2021-10-30 09:58:41');
INSERT INTO `eb_system_config` (`config_classify_id`, `config_name`, `config_key`, `config_type`, `config_rule`, `required`, `info`, `sort`, `user_type`, `status`, `create_time`) VALUES (61, '图文免审核', 'community_audit', 'radio', '0:必需审核\n1:无需审核', 0, '', 0, 0, 1, '2021-10-30 10:01:33');
INSERT INTO `eb_system_config` (`config_classify_id`, `config_name`, `config_key`, `config_type`, `config_rule`, `required`, `info`, `sort`, `user_type`, `status`, `create_time`) VALUES (61, '允许发帖用户', 'community_auth', 'radio', '0:全部用户\n1:绑定手机号用户', 0, '', 0, 0, 1, '2021-10-30 10:04:32');
INSERT INTO `eb_system_config` (`config_classify_id`, `config_name`, `config_key`, `config_type`, `config_rule`, `required`, `info`, `sort`, `user_type`, `status`, `create_time`) VALUES (61, '社区评论开关', 'community_reply_status', 'radio', '0:关闭\n1:开启', 0, '', 0, 0, 1, '2021-11-03 15:50:47');
INSERT INTO `eb_system_group` (`group_name`, `group_info`, `group_key`, `fields`, `user_type`, `sort`, `create_time`) VALUES ('社区热门搜索', '', 'community_hot_keyword', '[{\"name\":\"\\u5173\\u952e\\u8bcd\",\"field\":\"keyword\",\"type\":\"input\",\"param\":\"\"}]', 0, 0, '2021-10-29 14:22:57');
INSERT INTO `eb_system_config_classify` (`config_classify_id`, `pid`, `classify_name`, `classify_key`, `info`, `sort`, `create_time`, `icon`, `status`) VALUES (61, 0, '社区配置', 'community', '', 0, '2021-10-30 09:57:24', '', 1);
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1594, 785, '785/', '', '直播助手', '/marketing/studio/assistant', '[]', 0, 1, 1, 1, '2021-11-04 09:31:03', '2021-11-04 09:45:59');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1592, 1578, '1594/1578/', '', '删除', 'merchantBroadcastAssistantDelete', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-08 15:15:25');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1591, 1578, '1594/1578/', '', '备注', 'merchantBroadcastAssistantMark', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-08 15:15:25');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1590, 1578, '1594/1578/', '', '编辑', 'merchantBroadcastAssistantUpdate', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-08 15:15:25');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1589, 1578, '1594/1578/', '', '编辑表单', 'merchantBroadcastAssistantUpdateForm', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-08 15:15:25');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1588, 1578, '1594/1578/', '', '添加', 'merchantBroadcastAssistantCreate', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-08 15:15:25');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1587, 1578, '1594/1578/', '', '添加表单', 'merchantBroadcastAssistantCreateForm', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-08 15:15:25');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1586, 1578, '1594/1578/', '', '列表', 'merchantBroadcastAssistantLst', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-08 15:15:25');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1585, 882, '/106/785/786/881/882/', '', '消息推送', 'merchantBroadcastPushMessage', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1584, 882, '/106/785/786/881/882/', '', '添加小助手', 'merchantBroadcastAddAssistant', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1583, 882, '/106/785/786/881/882/', '', '添加小助手Form', 'merchantBroadcastAddAssistantForm', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1582, 882, '/106/785/786/881/882/', '', '商品上下架', 'merchantBroadcastOnSale', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1581, 882, '/106/785/786/881/882/', '', '收录开关', 'merchantBroadcastRoomCloseFeeds', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1580, 882, '/106/785/786/881/882/', '', '禁言开关', 'merchantBroadcastRoomCloseComment', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1579, 882, '/106/785/786/881/882/', '', '客服开关', 'merchantBroadcastRoomCloseKf', '', 1, 1, 1, 0, '2021-11-03 17:49:46', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1578, 1594, '1594/', '', '权限', '/', '', 1, 0, 1, 0, '2021-11-03 17:49:35', '2021-11-08 15:15:25');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1577, 1547, '/1538/1543/1547/', '', '删除', 'systemCommunityReplyDelete', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:13:01');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1576, 1547, '/1538/1543/1547/', '', '列表', 'systemCommunityReplyLst', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:13:01');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1575, 1546, '/1538/1541/1546/', '', '编辑状态', 'systemCommunityTopicStatus', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1574, 1546, '/1538/1541/1546/', '', '删除', 'systemCommunityTopicDelete', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1573, 1546, '/1538/1541/1546/', '', '详情', 'systemCommunityTopicDetail', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1572, 1546, '/1538/1541/1546/', '', '编辑', 'systemCommunityTopicUpdate', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1571, 1546, '/1538/1541/1546/', '', '编辑表单', 'systemCommunityTopicUpdateForm', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1570, 1546, '/1538/1541/1546/', '', '添加', 'systemCommunityTopicCreate', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1569, 1546, '/1538/1541/1546/', '', '添加表单', 'systemCommunityTopicCreateForm', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1568, 1546, '/1538/1541/1546/', '', '列表', 'systemCommunityTopicLst', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1567, 1545, '/1538/1540/1545/', '', '编辑状态', 'systemCommunityCategoryStatus', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:20');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1566, 1545, '/1538/1540/1545/', '', '删除', 'systemCommunityCategoryDelete', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:20');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1565, 1545, '/1538/1540/1545/', '', '详情', 'systemCommunityCategoryDetail', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:19');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1564, 1545, '/1538/1540/1545/', '', '编辑', 'systemCommunityCategoryUpdate', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:19');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1563, 1545, '/1538/1540/1545/', '', '编辑表单', 'systemCommunityCategoryUpdateForm', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:19');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1562, 1545, '/1538/1540/1545/', '', '添加', 'systemCommunityCategoryCreate', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:19');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1561, 1545, '/1538/1540/1545/', '', '添加表单', 'systemCommunityCategoryCreateForm', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:19');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1560, 1545, '/1538/1540/1545/', '', '列表', 'systemCommunityCategoryLst', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:12:19');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1559, 1546, '/1538/1541/1546/', '', '是否推荐', 'systemCommunityTopicHot', '', 1, 0, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:13:40');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1558, 1544, '/1538/1539/1544/', '', '是否显示', 'systemCommunityShow', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:11:57');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1557, 1544, '/1538/1539/1544/', '', '编辑状态', 'systemCommunityStatus', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:11:57');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1556, 1544, '/1538/1539/1544/', '', '编辑状态', 'systemCommunityStatusForm', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:11:57');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1555, 1544, '/1538/1539/1544/', '', '删除', 'systemCommunityDelete', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:11:57');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1554, 1544, '/1538/1539/1544/', '', '编辑', 'systemCommunityUpdate', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:11:56');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1553, 1544, '/1538/1539/1544/', '', '编辑表单', 'systemCommunityUpdateForm', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:11:56');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1552, 1544, '/1538/1539/1544/', '', '详情', 'systemCommunityDetail', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:11:56');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1551, 1544, '/1538/1539/1544/', '', '列表', 'systemCommunityLst', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:11:56');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1550, 815, '/719/782/781/814/815/', '', '收录开关', 'systemBroadcastRoomClosesFeeds', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1549, 815, '/719/782/781/814/815/', '', '禁言开关', 'systemBroadcastRoomCloseComment', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1548, 815, '/719/782/781/814/815/', '', '客服开关', 'systemBroadcastRoomCloseKf', '', 1, 1, 0, 0, '2021-11-03 17:49:23', '2021-11-03 18:00:26');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1547, 1543, '/1538/1543/', '', '权限', '/', '', 1, 0, 0, 0, '2021-11-03 17:49:23', '2021-11-08 15:13:01');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1546, 1541, '/1538/1541/', '', '权限', '/', '', 1, 0, 0, 0, '2021-11-03 17:49:22', '2021-11-08 15:12:42');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1545, 1540, '/1538/1540/', '', '权限', '/', '', 1, 0, 0, 0, '2021-11-03 17:49:22', '2021-11-08 15:12:20');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1544, 1539, '/1538/1539/', '', '权限', '/', '', 1, 0, 0, 0, '2021-11-03 17:49:21', '2021-11-08 15:11:57');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1543, 1538, '/1538/', '', '社区评论', '/community/reply', '[]', 7, 1, 0, 1, '2021-10-27 11:34:19', '2021-10-27 11:35:23');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1542, 1538, '/1538/', '', '社区配置', '/systemForm/Basics/community', '[]', 0, 1, 0, 1, '2021-10-27 11:32:22', '2021-11-02 18:09:21');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1541, 1538, '/1538/', '', '社区话题', '/community/topic', '[]', 9, 1, 0, 1, '2021-10-27 11:31:23', '2021-10-27 11:31:22');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1540, 1538, '/1538/', '', '社区分类', '/community/category', '[]', 10, 1, 0, 1, '2021-10-27 11:29:56', '2021-10-27 11:30:50');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1539, 1538, '/1538/', '', '社区文章', '/community/list', '[]', 8, 1, 0, 1, '2021-10-27 11:29:27', '2021-10-27 11:36:00');
INSERT INTO `eb_system_menu` (`menu_id`, `pid`, `path`, `icon`, `menu_name`, `route`, `params`, `sort`, `is_show`, `is_mer`, `is_menu`, `create_time`, `update_time`) VALUES (1538, 0, '/', 'guide', '社区', '/community', '[]', 96, 1, 0, 1, '2021-10-27 11:27:50', '2021-10-27 11:28:30');

CREATE TABLE `eb_broadcast_assistant`  (
  `assistant_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NULL DEFAULT NULL COMMENT '微信号',
  `nickname` varchar(100) NULL DEFAULT NULL COMMENT '微信昵称',
  `mer_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `mark` varchar(255) NULL DEFAULT NULL COMMENT '备注',
  `is_del` tinyint(1) NULL DEFAULT 0,
  UNIQUE INDEX `id`(`assistant_id`) USING BTREE
) ENGINE = InnoDB COMMENT = '直播助手信息';

CREATE TABLE `eb_community`  (
  `community_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NULL DEFAULT NULL COMMENT '标题',
  `image` varchar(1000) NULL DEFAULT NULL COMMENT '图片',
  `category_id` int(11) UNSIGNED NULL DEFAULT 0,
  `topic_id` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '话题',
  `uid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '用户',
  `count_start` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '点赞数',
  `count_reply` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '评论数',
  `count_share` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '分享数',
  `status` tinyint(2) NULL DEFAULT 0 COMMENT '审核状态',
  `is_show` tinyint(2) NULL DEFAULT 0 COMMENT '显示状态',
  `start` tinyint(1) NULL DEFAULT 1 COMMENT '星级排序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_del` tinyint(1) NULL DEFAULT 0,
  `content` varchar(500) NULL DEFAULT NULL,
  `refusal` varchar(255) NULL DEFAULT NULL COMMENT '拒绝理由',
  `is_hot` tinyint(2) NULL DEFAULT 0 COMMENT '是否推荐',
  PRIMARY KEY (`community_id`) USING BTREE
) ENGINE = InnoDB COMMENT = '社区图文表信息';

CREATE TABLE `eb_community_category`  (
  `category_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(50) NULL DEFAULT NULL COMMENT '分类名',
  `pid` int(11) NULL DEFAULT NULL COMMENT '父级ID',
  `path` varchar(255) NULL DEFAULT '/' COMMENT '路径 ',
  `is_show` tinyint(2) NULL DEFAULT 1 COMMENT '状态',
  `level` int(11) NULL DEFAULT 0 COMMENT '等级',
  `sort` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB COMMENT = '社区分类';

CREATE TABLE `eb_community_reply`  (
  `reply_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NULL DEFAULT NULL COMMENT '评论内容',
  `pid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '回复id',
  `uid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '发言人',
  `re_uid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '回复人',
  `count_start` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '点赞数',
  `count_reply` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '评论数',
  `status` tinyint(2) NULL DEFAULT 1 COMMENT '状态 ',
  `community_id` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '文章id',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_del` tinyint(2) NULL DEFAULT 0,
  PRIMARY KEY (`reply_id`) USING BTREE,
  UNIQUE INDEX `id`(`reply_id`) USING BTREE
) ENGINE = InnoDB COMMENT = '社区评论';

CREATE TABLE `eb_community_topic`  (
  `topic_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(100) NULL DEFAULT NULL COMMENT '话题',
  `status` tinyint(2) NULL DEFAULT 1 COMMENT '状态',
  `is_hot` tinyint(11) NULL DEFAULT 0 COMMENT '推荐',
  `category_id` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '分类id',
  `is_del` tinyint(2) NULL DEFAULT 0,
  `pic` varchar(128) NULL DEFAULT NULL COMMENT '图标',
  `count_use` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '使用次数',
  `count_view` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '浏览量',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sort` int(11) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`topic_id`) USING BTREE,
  UNIQUE INDEX `id`(`topic_id`) USING BTREE
) ENGINE = InnoDB COMMENT = '社区话题';

CREATE TABLE `eb_relevance`  (
  `relevance_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `left_id` int(11) UNSIGNED NOT NULL,
  `right_id` int(11) UNSIGNED NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`relevance_id`) USING BTREE,
  INDEX `type`(`type`, `left_id`, `right_id`) USING BTREE
) ENGINE = InnoDB;

ALTER TABLE `eb_broadcast_room` ADD COLUMN `feeds_img` varchar(255) NULL DEFAULT NULL COMMENT '封面图' AFTER `is_mer_del`;
ALTER TABLE `eb_broadcast_room` ADD COLUMN `push_url` varchar(255) NULL DEFAULT NULL COMMENT '推流地址' AFTER `feeds_img`;
ALTER TABLE `eb_broadcast_room` ADD COLUMN `assistant_id` varchar(255) NULL DEFAULT NULL COMMENT '小助手ID' AFTER `push_url`;
ALTER TABLE `eb_broadcast_room` ADD COLUMN `is_feeds_public` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否开启官方收录，1 开启，0 关闭' AFTER `assistant_id`;
ALTER TABLE `eb_broadcast_room_goods` ADD COLUMN `on_sale` tinyint(2) NULL DEFAULT 1 COMMENT '商品上下架' AFTER `broadcast_goods_id`;
ALTER TABLE `eb_store_cart` ADD COLUMN `spread_id` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '推广人' AFTER `is_fail`;
ALTER TABLE `eb_store_order` ADD COLUMN `spread_uid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '推荐人id' AFTER `uid`;
ALTER TABLE `eb_store_order` ADD COLUMN `top_uid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '二级推荐人 id' AFTER `spread_uid`;
ALTER TABLE `eb_store_product_assist_set` MODIFY COLUMN `create_time` timestamp NOT NULL COMMENT '时间' AFTER `yet_assist_count`;
ALTER TABLE `eb_user` ADD COLUMN `count_start` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '用户获赞数' AFTER `member_value`;
ALTER TABLE `eb_user` ADD COLUMN `count_fans` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '用户粉丝数' AFTER `count_start`;

INSERT INTO `eb_community_category` (`category_id`, `cate_name`, `pid`, `path`, `is_show`, `level`, `sort`) VALUES (64, '家居', 0, '/', 1, 0, 0);
INSERT INTO `eb_community_category` (`category_id`, `cate_name`, `pid`, `path`, `is_show`, `level`, `sort`) VALUES (65, '美妆', 0, '/', 1, 0, 0);
INSERT INTO `eb_community_category` (`category_id`, `cate_name`, `pid`, `path`, `is_show`, `level`, `sort`) VALUES (66, '美食', 0, '/', 1, 0, 0);
INSERT INTO `eb_community_category` (`category_id`, `cate_name`, `pid`, `path`, `is_show`, `level`, `sort`) VALUES (67, '穿搭', 0, '/', 1, 0, 0);
INSERT INTO `eb_community_category` (`category_id`, `cate_name`, `pid`, `path`, `is_show`, `level`, `sort`) VALUES (68, '旅游', 0, '/', 1, 0, 0);

INSERT INTO `eb_community_topic` (`topic_id`, `topic_name`, `status`, `is_hot`, `category_id`, `is_del`, `pic`, `count_use`, `count_view`, `create_time`, `sort`) VALUES (36, '设计感家居好物', 1, 1, 64, 0, 'https://mer1.crmeb.net/uploads/def/20211110/9b809928e69f46eddc7eaceef8f40347.jpg', 0, 0, '2021-11-10 09:36:39', 0);
INSERT INTO `eb_community_topic` (`topic_id`, `topic_name`, `status`, `is_hot`, `category_id`, `is_del`, `pic`, `count_use`, `count_view`, `create_time`, `sort`) VALUES (37, '职场穿搭', 1, 1, 67, 0, 'https://mer1.crmeb.net/uploads/def/20211110/f8db48357bd42307d51ef60905ef8516.jpg', 0, 0, '2021-11-10 09:37:33', 0);
INSERT INTO `eb_community_topic` (`topic_id`, `topic_name`, `status`, `is_hot`, `category_id`, `is_del`, `pic`, `count_use`, `count_view`, `create_time`, `sort`) VALUES (38, '早餐吃什么', 1, 1, 66, 0, 'https://mer1.crmeb.net/uploads/def/20211110/6968c4026ff623ce73bed19612c21dfb.jpg', 0, 0, '2021-11-10 09:37:57', 0);
INSERT INTO `eb_community_topic` (`topic_id`, `topic_name`, `status`, `is_hot`, `category_id`, `is_del`, `pic`, `count_use`, `count_view`, `create_time`, `sort`) VALUES (39, '养猫那些事', 1, 0, 68, 0, 'https://mer1.crmeb.net/uploads/def/20211110/e7bcce709b1eaebe63350e56b34d56ae.jpg', 0, 0, '2021-11-10 09:38:16', 0);
