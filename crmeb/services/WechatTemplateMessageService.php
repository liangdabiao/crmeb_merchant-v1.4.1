<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace crmeb\services;

use app\common\repositories\store\order\StoreGroupOrderRepository;
use app\common\repositories\store\order\StoreOrderRepository;
use app\common\repositories\store\order\StoreOrderStatusRepository;
use app\common\repositories\store\order\StoreRefundOrderRepository;
use app\common\repositories\store\product\ProductGroupBuyingRepository;
use app\common\repositories\store\product\ProductGroupUserRepository;
use app\common\repositories\store\product\ProductRepository;
use app\common\repositories\store\product\ProductTakeRepository;
use app\common\repositories\store\service\StoreServiceRepository;
use app\common\repositories\system\merchant\MerchantRepository;
use app\common\repositories\system\notice\SystemNoticeConfigRepository;
use app\common\repositories\user\UserBillRepository;
use app\common\repositories\user\UserExtractRepository;
use app\common\repositories\user\UserRechargeRepository;
use app\common\repositories\wechat\WechatUserRepository;
use crmeb\listens\pay\UserRechargeSuccessListen;
use crmeb\services\template\Template;
use app\common\repositories\user\UserRepository;
use think\facade\Route;

class WechatTemplateMessageService
{
    /**
     * TODO
     * @param array $data
     * @param string|null $link
     * @param string|null $color
     * @return bool
     * @author Qinii
     * @day 2020-06-29
     */
    public  function sendTemplate(array $data)
    {
        event('wechat.template.before',compact('data'));
        $res = $this->templateMessage($data['tempCode'],$data['id'], $data['data'] ?? []);
        if(!$res || !is_array($res))
            return true;
        foreach($res as $item){
            if(is_array($item['uid'])){
                foreach ($item['uid'] as $value){
                    $openid = $this->getUserOpenID($value['uid']);
                    if (!$openid) {
                        continue;
                    }
                    $this->send($openid,$item['tempCode'],$item['data'],'wechat',$item['link'],$item['color']);
                }
            }else{
                $openid = $this->getUserOpenID($item['uid']);
                if (!$openid) {
                    continue;
                }
                $this->send($openid,$item['tempCode'],$item['data'],'wechat',$item['link'],$item['color']);
            }
        }
        event('wechat.template',compact('res'));
    }

    /**
     * TODO
     * @param $data
     * @param string|null $link
     * @param string|null $color
     * @return bool
     * @author Qinii
     * @day 2020-07-01
     */
    public function subscribeSendTemplate($data)
    {
        event('wechat.subscribeTemplate.before',compact('data'));
        $res = $this->subscribeTemplateMessage($data['tempCode'],$data['id']);
        if(!$res || !is_array($res))return true;

        foreach($res as $item){
            if(is_array($item['uid'])){
                foreach ($item['uid'] as $value){
                    $openid = $this->getUserOpenID($value,'min');
                    if (!$openid) {
                        continue;
                    }
                    $this->send($openid,$item['tempCode'],$item['data'],'subscribe',$item['link'],$item['color']);
                }
            }else{
                $openid = $this->getUserOpenID($item['uid'],'min');
                if (!$openid) {
                    continue;
                }
                $this->send($openid,$item['tempCode'],$item['data'],'subscribe',$item['link'],$item['color']);
            }
        }
        event('wechat.subscribeTemplate',compact('res'));
    }

    /**
     * TODO
     * @param $uid
     * @return mixed
     * @author Qinii
     * @day 2020-06-29
     */
    public function getUserOpenID($uid,$type = 'wechat')
    {
        $user = app()->make(UserRepository::class)->get($uid);
        $make = app()->make(WechatUserRepository::class);
        if($type == 'wechat') {
            return $make->idByOpenId((int)$user['wechat_user_id']);
        }else{
            return $make->idByRoutineId((int)$user['wechat_user_id']);
        }
    }


    /**
     * TODO
     * @param $openid
     * @param $tempCode
     * @param $data
     * @param $type
     * @param $link
     * @param $color
     * @return bool|mixed
     * @author Qinii
     * @day 2020-07-01
     */
    public function send($openid,$tempCode,$data,$type,$link,$color)
    {
        try{
            $template = new Template($type);
            $template->to($openid)->color($color);
            if ($link) $template->url($link);
            return $template->send($tempCode, $data);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * TODO 公众号
     * @param string $tempCode
     * @param $id
     * @return array|bool
     * @author Qinii
     * @day 2020-07-01
     */
    public  function templateMessage(string $tempCode,  $id, $params = [])
    {
        $bill_make = app()->make(UserBillRepository::class);
        $order_make = app()->make(StoreOrderRepository::class);
        $refund_make = app()->make(StoreRefundOrderRepository::class);
        $order_status_make = app()->make(StoreOrderStatusRepository::class);
        $notice_make = app()->make(SystemNoticeConfigRepository::class);
        switch ($tempCode)
        {
            case 'ORDER_CREATE': //订单生成通知
                if (!$notice_make->getNoticeWechat('order_success')) return false;
                $res = $order_make->selectWhere(['group_order_id' => $id]);
                if(!$res) return false;
                foreach ($res as $item){
                    $order = $order_make->getWith($item['order_id'],'orderProduct');
                    $data[] = [
                        'tempCode' => 'ORDER_CREATE',
                        'uid' => app()->make(StoreServiceRepository::class)->getNoticeServiceInfo($item->mer_id),
                        'data' => [
                            'first' => '您有新的生成订单请注意查看',
                            'keyword1' => $item->create_time,
                            'keyword2' => '「' . $order['orderProduct'][0]['cart_info']['product']['store_name'] . '」等',
                            'keyword3' => $item->order_sn,
                            'remark' => '查看详情'
                        ],
                        'link' => rtrim(systemConfig('site_url'), '/') . '/pages/admin/orderList/index?types=1&merId=' . $item->mer_id,
                        'color' => null
                    ];
                }
                break;
            case 'ORDER_PAY_SUCCESS': //支付成功

                if ($notice_make->getNoticeWechat('pay_status')) {
                    $group_order = app()->make(StoreGroupOrderRepository::class)->get($id);
                    if(!$group_order) return false;
                    $data[] = [
                        'tempCode' => 'ORDER_PAY_SUCCESS',
                        'uid' => $group_order->uid,
                        'data' => [
                            'first' => '您的订单已支付',
                            'keyword1' => $group_order->group_order_sn,
                            'keyword2' => $group_order->pay_price,
                            'remark' => '我们会尽快发货，请耐心等待'
                        ],
                        'link' => rtrim(systemConfig('site_url'), '/') . '/pages/users/order_list/index?status=1',
                        'color' => null
                    ];
                }
                if ($notice_make->getNoticeWechat('admin_pay_status')) {
                    $res = $order_make->selectWhere(['group_order_id' => $id]);
                    if (!$res) return false;
                    foreach ($res as $item) {
                        $data[] = [
                            'tempCode' => 'ORDER_PAY_SUCCESS',
                            'uid' => app()->make(StoreServiceRepository::class)->getNoticeServiceInfo($item->mer_id),
                            'data' => [
                                'first' => '您有新的支付订单请注意查看。',
                                'keyword1' => $item->order_sn,
                                'keyword2' => $item->pay_price,
                                'remark' => '请尽快发货。'
                            ],
                            'link' => rtrim(systemConfig('site_url'), '/') . '/pages/admin/orderList/index?types=2&merId=' . $item->mer_id,
                            'color' => null
                        ];
                    }
                }
                break;
            case 'ORDER_POSTAGE_SUCCESS'://订单发货提醒(快递)
                if (!$notice_make->getNoticeWechat('fahuo_status')) return false;
                $res = $order_make->get($id);
                if(!$res) return false;
                $data[] = [
                    'tempCode' => 'ORDER_POSTAGE_SUCCESS',
                    'uid' =>  $res->uid ,
                    'data' => [
                        'first' => '亲，宝贝已经启程了，好想快点来到你身边',
                        'keyword1' => $res['order_sn'],
                        'keyword2' => $res['delivery_name'],
                        'keyword3' => $res['delivery_id'],
                        'remark' => '请耐心等待收货哦。'
                    ],
                    'link' => rtrim(systemConfig('site_url'),'/').'/pages/order_details/index?order_id='.$id,
                    'color' => null
                ];
                break;
            case 'ORDER_DELIVER_SUCCESS'://订单发货提醒(送货)
                if (!$notice_make->getNoticeWechat('fahuo_status')) return false;
                $res = $order_make->getWith($id,'orderProduct');
                if(!$res) return false;
                $data[] = [
                    'tempCode' => 'ORDER_DELIVER_SUCCESS',
                    'uid' =>  $res->uid ,
                    'data' => [
                        'first' => '亲，宝贝已经启程了，好想快点来到你身边',
                        'keyword1' => '「'.$res['orderProduct'][0]['cart_info']['product']['store_name'].'」等',
                        'keyword2' => $res['create_time'],
                        'keyword3' => $res['user_address'],
                        'keyword4' => $res['delivery_name'],
                        'keyword5' => $res['delivery_id'],
                        'remark' => '请耐心等待收货哦。'
                    ],
                    'link' => rtrim(systemConfig('site_url'),'/').'/pages/order_details/index?order_id='.$id,
                    'color' => null
                ];
                break;

            case 'ORDER_TAKE_SUCCESS': //订单收货通知
                if (!$notice_make->getNoticeWechat('take_status')) return false;
                $res = $order_make->getWith($id,'orderProduct');
                if(!$res) return false;
                $status = $order_status_make->getWhere(['order_id' => $id,'change_type' => 'take']);
                $data[] = [
                    'tempCode' => 'ORDER_TAKE_SUCCESS',
                    'uid' => $res->uid,
                    'data' => [
                        'first' => '亲，宝贝已经签收',
                        'keyword1' => $res['order_sn'],
                        'keyword2' => '已收货',
                        'keyword3' => $status['change_time'],
                        'keyword4' => '「'.$res['orderProduct'][0]['cart_info']['product']['store_name'].'」等',
                        'remark' => '请确认。'
                    ],
                    'link' => rtrim(systemConfig('site_url'),'/').'/pages/order_details/index?order_id='.$id,
                    'color' => null
                ];
                break;
            case 'USER_BALANCE_CHANGE'://帐户资金变动提醒
                if (!$notice_make->getNoticeWechat('user_bill_change')) return false;
                $res = $bill_make->get($id);
                if(!$res) return false;
                $data[] = [
                    'tempCode' => 'USER_BALANCE_CHANGE',
                    'uid' => $res->uid,
                    'data' => [
                        'first' => '资金变动提醒',
                        'keyword1' => '账户余额变动',
                        'keyword2' => $res['number'],
                        'keyword3' => $res['create_time'],
                        'keyword4' => $res['balance'],
                        'remark' => '请确认'
                    ],
                    'link' => rtrim(systemConfig('site_url'),'/').'/pages/users/user_money/index',
                    'color' => null
                ];
                break;

            case 'ORDER_REFUND_STATUS'://退款申请通知
                if (!$notice_make->getNoticeWechat('refund_order_create')) return false;
                $res = $refund_make->get($id);
                if(!$res) return false;
                $data[] = [
                    'tempCode' => 'ORDER_REFUND_STATUS',
                    'uid' => app()->make(StoreServiceRepository::class)->getNoticeServiceInfo($res->mer_id),
                    'data' => [
                        'first' => '您有新的退款申请',
                        'keyword1' => $res['refund_order_sn'],
                        'keyword2' => $res['refund_price'],
                        'keyword3' => $res['refund_message'],
                        'remark' => '请及时处理'
                    ],
                    'link' => null,
                    'color' => null
                ];
                break;
            case 'ORDER_REFUND_END'://退货确认提醒
                if (!$notice_make->getNoticeWechat('refund_confirm_status')) return false;
                $res = $refund_make->getWith($id,['order']);
                if(!$res) return false;
                $order = $order_make->getWith($res['order_id'],'orderProduct');
                $data[] = [
                    'tempCode' => 'ORDER_REFUND_END',
                    'uid' => $res->uid,
                    'data' => [
                        'first' => '亲，您有一个订单已退款',
                        'keyword1' => $res['refund_order_sn'],
                        'keyword2' => $res['order']['order_sn'],
                        'keyword3' => $res['refund_price'],
                        'keyword4' => '「'.$order['orderProduct'][0]['cart_info']['product']['store_name'].'」等',
                        'remark' => $order['activity_type'] == 4 ? '拼团失败，系统自动退款' : '请查看详情'
                    ],
                    'link' => rtrim(systemConfig('site_url'),'/').'/pages/users/refund/detail?id='.$id,
                    'color' => null
                ];
                break;

            case 'ORDER_REFUND_NOTICE'://退款进度提醒
                $status = [-1=>'审核未通过' ,1 => '商家已同意退货，请尽快将商品退回，并填写快递单号',];
                $res = $refund_make->getWith($id,['order']);
                if(!$res || !in_array($res['status'],[-1,1])) return false;
                if ($res['status']  == 1 && !$notice_make->getNoticeWechat('refund_success_status')) return false;
                if ($res['status']  == -1 && !$notice_make->getNoticeWechat('refund_fail_status')) return false;
                $order = $order_make->getWith($res['order_id'],'orderProduct');
                $data[] = [
                    'tempCode' => 'ORDER_REFUND_NOTICE',
                    'uid' => $res->uid,
                    'data' => [
                        'first' => '退款进度提醒',
                        'keyword1' => $res['refund_order_sn'],
                        'keyword2' => $status[$res['status']],
                        'keyword3' => '「'.$order['orderProduct'][0]['cart_info']['product']['store_name'].'」等',
                        'keyword4' => $res['refund_price'],
                        'remark' => ''
                    ],
                    'link' => rtrim(systemConfig('site_url'),'/').'/pages/users/refund/detail?id='.$id,
                    'color' => null
                ];
                break;
            case 'GROUP_BUYING_SUCCESS':
                /*
                 {{first.DATA}}
                商品名称：{{keyword1.DATA}}
                订单号：{{keyword2.DATA}}
                支付金额：{{keyword3.DATA}}
                支付时间：{{keyword4.DATA}}
                {{remark.DATA}}
                 */
                if (!$notice_make->getNoticeWechat('group_buying_success')) return false;
                $res = app()->make(ProductGroupBuyingRepository::class)->get($id);
                if(!$res) return false;
                $buying_make = app()->make(ProductGroupUserRepository::class);
                $ret = $buying_make->getSearch(['group_buying_id' => $id])->where('uid','>',0)->select();
                foreach ($ret as $item){
                    $data[] = [
                        'tempCode' => 'GROUP_BUYING_SUCCESS',
                        'uid' => $item->uid,
                        'data' => [
                            'first' => '恭喜您拼团成功!',
                            'keyword1' => '「'.$res->productGroup->product['store_name'].'」',
                            'keyword2' => $item->orderInfo['order_sn'],
                            'keyword3' => $item->orderInfo['pay_price'],
                            'keyword4' => $item->orderInfo['pay_time'],
                            'remark' => ''
                        ],
                        'link' => rtrim(systemConfig('site_url'),'/').'/pages/order_details/index?order_id='.$item['order_id'],
                        'color' => null
                    ];
                }
                break;
            case'PRODUCT_INCREASE':
                /*
                    {{first.DATA}}
                    预订商品：{{keyword1.DATA}}
                    到货数量：{{keyword2.DATA}}
                    到货时间：{{keyword3.DATA}}
                    {{remark.DATA}}
                 */
                if (!$notice_make->getNoticeWechat('prodcut_increase_status')) return false;
                $make = app()->make(ProductTakeRepository::class);
                $product = app()->make(ProductRepository::class)->getWhere(['product_id' => $id],'*',['attrValue']);
                if(!$product) return false;
                $unique[] = 1;
                foreach ($product['attrValue'] as $item) {
                    if($item['stock'] > 0){
                        $unique[] = $item['unique'];
                    }
                }
                $query = $make->getSearch(['product_id' => $id,'status' =>0,'type' => 2])->where('unique','in',$unique);
                $uid = $query->column('uid,product_id');
                if(!$uid) return false;
                $tak_id = $query->column('product_take_id');
                app()->make(ProductTakeRepository::class)->updates($tak_id,['status' => 1]);

                $data[] = [
                    'tempCode' => 'PRODUCT_INCREASE',
                    'uid' => $uid,
                    'data' => [
                        'first' => '亲，你想要的商品已到货，可以购买啦~',
                        'keyword1' => '「'.$product->store_name.'」',
                        'keyword2' => $product->stock,
                        'keyword3' => date('Y-m-d H:i:s',time()),
                        'remark' => ''
                    ],
                    'link' => rtrim(systemConfig('site_url'),'/').'/pages/goods_details/index?id='.$id,
                    'color' => null
                ];
                break;
            case'SERVER_NOTICE':
                /*
                {{first.DATA}}
                访客姓名：{{keyword1.DATA}}
                联系方式：{{keyword2.DATA}}
                项目名称：{{keyword3.DATA}}
                {{remark.DATA}}
                 */
                if (!$notice_make->getNoticeWechat('server_notice')) return false;
                $mer = app()->make(MerchantRepository::class)->get($params['mer_id']);
                $data[] = [
                    'tempCode' => 'SERVER_NOTICE',
                    'uid' => $id,
                    'data' => [
                        'first' => '亲，您有新的消息请注意查看~',
                        'keyword1' => $params['keyword1'],
                        'keyword2' => $mer['mer_name'],
                        'keyword3' => $params['keyword3'],
                        'remark' => ''
                    ],
                    'link' => $params['url'],
                    'color' => null
                ];
                break;
            default:
                return false;
                break;
        }
        return $data;
    }

    /**
     * TODO 小程序模板
     * @param string $tempCode
     * @param $id
     * @return array|bool
     * @author Qinii
     * @day 2020-07-01
     */
    public function subscribeTemplateMessage(string $tempCode, $id)
    {
        $user_make = app()->make(UserRechargeRepository::class);
        $order_make = app()->make(StoreOrderRepository::class);
        $refund_make = app()->make(StoreRefundOrderRepository::class);
        $order_group_make = app()->make(StoreGroupOrderRepository::class);
        $extract_make = app()->make(UserExtractRepository::class);
        $notice_make = app()->make(SystemNoticeConfigRepository::class);
        switch($tempCode)
        {
            case 'ORDER_PAY_SUCCESS': //订单支付成功
                if(!$notice_make->getNoticeRoutine('pay_status')) return false;
                $res = $order_group_make->get($id);
                if(!$res) return false;
                $data[] = [
                    'tempCode' => 'ORDER_PAY_SUCCESS',
                    'uid' => $res->uid,
                    'data' => [
                        'character_string1' => $res->group_order_sn,
                        'amount2' => $res->pay_price,
                        'date3' => $res->pay_time,
                        'amount5' => $res->total_price,
                    ],
                    'link' => 'pages/users/order_list/index?status=1',
                    'color' => null
                ];
                break;
            case 'ORDER_DELIVER_SUCCESS':  //订单发货提醒(送货)
                if(!$notice_make->getNoticeRoutine('fahuo_status')) return false;
                $res = $order_make->getWith($id,'orderProduct');
                if(!$res) return false;
                $name = substr($res['orderProduct'][0]['cart_info']['product']['store_name'],0,10);
                $data[] = [
                    'tempCode' => 'ORDER_DELIVER_SUCCESS',
                    'uid' => $res->uid,
                    'data' => [
                        'thing8' => '「'.$name.'」等',
                        'character_string1' => $res->order_sn,
                        'name4' => $res->delivery_name,
                        'phone_number10' => $res->delivery_id,
                    ],
                    'link' => 'pages/order_details/index?order_id='.$id,
                    'color' => null
                ];
                break;
            case 'ORDER_POSTAGE_SUCCESS': //订单发货提醒(快递)
                if(!$notice_make->getNoticeRoutine('fahuo_status')) return false;
                $res = $order_make->getWith($id,'orderProduct');
                if(!$res) return false;
                $name = substr($res['orderProduct'][0]['cart_info']['product']['store_name'],0,10);
                $data[] = [
                    'tempCode' => 'ORDER_POSTAGE_SUCCESS',
                    'uid' => $res->uid,
                    /**
                    快递单号{{character_string2.DATA}}
                    快递公司{{thing1.DATA}}
                    发货时间{{time3.DATA}}
                    订单商品{{thing5.DATA}}
                     */

                    'data' => [
                        'character_string2' => $res->delivery_id,
                        'thing1' => $res->delivery_name,
                        'time3' => date('Y-m-d H:i:s',time()),
                        'thing5' => '「'.$name.'」等',
                    ],

                    'link' => 'pages/order_details/index?order_id='.$id,
                    'color' => null
                ];
                break;
            case 'ORDER_REFUND_NOTICE': //退款通知
                $status = [-1=>'审核未通过' ,1 => '商家已同意退货，请尽快将商品退回',3 => '退款成功'];
                $res = $refund_make->getWith($id,['order']);
                if(!$res || !in_array($res['status'],[-1,1,3])) return false;
                if($res['status'] == -1 && !$notice_make->getNoticeRoutine('refund_fail_status')) return false;
                if($res['status'] == 1  && !$notice_make->getNoticeRoutine('refund_success_status')) return false;
                if($res['status'] == 3  && !$notice_make->getNoticeRoutine('refund_confirm_status')) return false;
                $order = $order_make->getWith($res['order_id'],'orderProduct');
                $name = substr($order['orderProduct'][0]['cart_info']['product']['store_name'],0,10);
                $data[] = [
                    'tempCode' => 'ORDER_REFUND_NOTICE',
                    'uid' => $res->uid,
                    'data' => [
                        'thing1' => $status[$res->status],
                        'thing2' => '「'.$name.'」等',
                        'character_string6' => $res->refund_order_sn,
                        'amount3' => $res->refund_price,
                        'thing13' => $res->fail_message ?? '',
                    ],
                    'link' => 'pages/users/refund/detail?id='.$id,
                    'color' => null
                ];
                break;
            case 'RECHARGE_SUCCESS': //充值成功
                if(!$notice_make->getNoticeRoutine('user_bill_change')) return false;
                $res = $user_make->get($id);
                if(!$res) return false;
                $data[] = [
                    'tempCode' => 'RECHARGE_SUCCESS',
                    'uid' => $res->uid,
                    'data' => [
                        'character_string1' => $res->order_id,
                        'amount3' => $res->price,
                        'amount6' => $res->give_price,
                        'date5' => $res->pay_time,
                    ],
                    'link' => 'pages/users/user_money/index',
                    'color' => null
                ];
                break;
            case 'USER_EXTRACT':  //提现结果通知
                if(!$notice_make->getNoticeRoutine('user_extract_status')) return false;
                $res = $extract_make->get($id);
                if(!$res) return false;
                $data[] = [
                    'tempCode' => 'USER_EXTRACT',
                    'uid' => $res->uid,
                    'data' => [
                        'thing1' => $res->status == -1 ? '未通过' : '已通过',
                        'amount2' => empty($res->bank_code)?(empty($res->alipay_code)?$res->wechat:$res->alipay_code):$res->bank_code,
                        'thing3' => $res->give_price,
                        'date4' => $res->create_time,
                    ],
                    'link' => 'pages/users/user_spread_user/index',
                    'color' => null
                ];
                break;
            case'PRODUCT_INCREASE':
                /*
                    商品名称 {{thing1.DATA}}
                    商品价格：{{amount5.DATA}}
                    温馨提示：{{thing2.DATA}}
                 */
                if(!$notice_make->getNoticeRoutine('prodcut_increase_status')) return false;
                $make = app()->make(ProductTakeRepository::class);
                $product = app()->make(ProductRepository::class)->getWhere(['product_id' => $id],'*',['attrValue']);
                if(!$product) return false;
                $unique[] = 1;
                foreach ($product['attrValue'] as $item) {
                    if($item['stock'] > 0){
                        $unique[] = $item['unique'];
                    }
                }
                $query = $make->getSearch(['product_id' => $id,'status' =>0,'type' => 3])->where('unique','in',$unique);
                $uid = $query->column('uid');
                if(!$uid) return false;
                $tak_id = $query->column('product_take_id');
                app()->make(ProductTakeRepository::class)->updates($tak_id,['status' => 1]);
                $data[] = [
                    'tempCode' => 'PRODUCT_INCREASE',
                    'uid' => $uid,
                    'data' => [
                        'thing1' => '「'.substr($product->store_name,0,10) .'」',
                        'amount5' => $product->price,
                        'thing2' => '亲！你想要的商品已到货，可以购买啦~',
                    ],
                    'link' => 'pages/goods_details/index?id='.$id,
                    'color' => null
                ];
                break;
            default:
                return false;
                break;
        }
       return $data;
    }
}
