<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace crmeb\services;


use app\common\dao\system\sms\SmsRecordDao;
use app\common\repositories\store\broadcast\BroadcastRoomRepository;
use app\common\repositories\store\order\StoreGroupOrderRepository;
use app\common\repositories\store\order\StoreOrderRepository;
use app\common\repositories\store\order\StoreRefundOrderRepository;
use app\common\repositories\store\product\ProductRepository;
use app\common\repositories\store\product\ProductTakeRepository;
use app\common\repositories\store\service\StoreServiceRepository;
use app\common\repositories\system\config\ConfigValueRepository;
use app\common\repositories\system\notice\SystemNoticeConfigRepository;
use crmeb\exceptions\SmsException;
use FormBuilder\Exception\FormBuilderException;
use FormBuilder\Factory\Elm;
use FormBuilder\Form;
use think\exception\ValidateException;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Route;

/**
 * Class YunxinSmsService
 * @package crmeb\services
 * @author xaboy
 * @day 2020-05-18
 */
class YunxinSmsService
{
    /**
     * api
     */
    const API = 'https://sms.crmeb.net/api/';
    // const API = 'http://plat.crmeb.net/api/';

    /**
     * @var array
     */
    protected $config;

    /**
     * YunxinSmsService constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
        if (isset($this->config['sms_token'])) {
            $this->config['sms_token'] = $this->getToken();
        }
    }

    public static function getNoticeStatus($tempId)
    {
        $make = app()->make(SystemNoticeConfigRepository::class);
        switch ($tempId) {
            case 'DELIVER_GOODS_CODE':
                return $make->getNoticeSms('fahuo_status');
                break;
            case 'TAKE_DELIVERY_CODE':
                return $make->getNoticeSms('take_status');
                break;
            case 'PAY_SUCCESS_CODE':
                return $make->getNoticeSms('pay_status');
                break;
            case 'PRICE_REVISION_CODE':
                return $make->getNoticeSms('revision_status');
                break;
            case 'ORDER_PAY_FALSE':
                return $make->getNoticeSms('pay_false_status');
                break;
            case 'REFUND_FAIL_CODE':
                return $make->getNoticeSms('refund_fail_status');
                break;
            case 'REFUND_SUCCESS_CODE':
                return $make->getNoticeSms('refund_success_status');
                break;
            case 'REFUND_CONFORM_CODE':
                return $make->getNoticeSms('refund_confirm_status');
                break;
            case 'ADMIN_PAY_SUCCESS_CODE':
                return $make->getNoticeSms('admin_pay_status');
                break;
            case 'ADMIN_RETURN_GOODS_CODE':
                return $make->getNoticeSms('admin_return_status');
                break;
            case 'ADMIN_TAKE_DELIVERY_CODE':
                return $make->getNoticeSms('admin_take_status');
                break;
            case 'ADMIN_DELIVERY_CODE':
                return $make->getNoticeSms('admin_postage_status');
                break;
            case 'BROADCAST_ROOM_CODE':
                return $make->getNoticeSms('broadcast_room_status');
                break;
            case 'BROADCAST_ROOM_FAIL':
                return $make->getNoticeSms('broadcast_room_fail');
                break;
            case 'PAY_PRESELL_CODE':
                return $make->getNoticeSms('pay_presell_status');
                break;
            case 'APPLY_MER_SUCCESS':
                return $make->getNoticeSms('apply_mer_succ_status');
                break;
            case 'APPLY_MER_FAIL':
                return $make->getNoticeSms('apply_mer_fail_status');
                break;
            case 'ARRIVAL_CODE':
                return $make->getNoticeSms('prodcut_increase_status');
                break;
            case 'INTEGRAL_INVALID':

                return $make->getNoticeSms('integral_status');
                break;
            default:
                return false;
                break;
        }
    }
    public static function sendMessage($tempId, $id)
    {
        if (!self::getNoticeStatus($tempId)) return;
        if ($tempId == 'DELIVER_GOODS_CODE') {
            $order = app()->make(StoreOrderRepository::class)->get($id);
            if (!$order || !$order->user_phone) return;
            $nickname = $order->user->nickname;
            $store_name = $order->orderProduct[0]['cart_info']['product']['store_name'] . (count($order->orderProduct) ? '等' : '');
            $order_id = $order->order_sn;
            self::create()->send($order->user_phone, $tempId, compact('nickname', 'store_name', 'order_id'));
        } else if ($tempId == 'TAKE_DELIVERY_CODE') {
            $order = app()->make(StoreOrderRepository::class)->get($id);
            if (!$order || !$order->user_phone) return;
            $order_id = $order->order_sn;
            $store_name = $order->orderProduct[0]['cart_info']['product']['store_name'] . (count($order->orderProduct) ? '等' : '');
            self::create()->send($order->user_phone, $tempId, compact('store_name', 'order_id'));
        } else if ($tempId == 'PAY_SUCCESS_CODE') {

            $order = app()->make(StoreGroupOrderRepository::class)->get($id);
            if (!$order || !$order->user_phone) return;
            $pay_price = $order->pay_price;
            $order_id = $order->group_order_sn;
            self::create()->send($order->user_phone, $tempId, compact('pay_price', 'order_id'));
        } else if ($tempId == 'PRICE_REVISION_CODE') {
            $order = app()->make(StoreOrderRepository::class)->get($id);
            if (!$order || !$order->user_phone) return;
            $pay_price = $order->pay_price;
            $order_id = $order->order_sn;
            self::create()->send($order->user_phone, $tempId, compact('pay_price', 'order_id'));
        } else if ($tempId == 'ORDER_PAY_FALSE') {
            $order = app()->make(StoreGroupOrderRepository::class)->get($id);
            if (!$order || !$order->user_phone) return;
            $order_id = $order->group_order_sn;
            self::create()->send($order->user_phone, $tempId, compact('order_id'));
        } else if ($tempId == 'REFUND_FAIL_CODE') {
            $order = app()->make(StoreRefundOrderRepository::class)->get($id);
            if (!$order || !$order->order->user_phone) return;
            $order_id = $order->order->order_sn;
            $store_name = $order->refundProduct[0]->product['cart_info']['product']['store_name'] . (count($order->refundProduct) ? '等' : '');
            self::create()->send($order->order->user_phone, $tempId, compact('order_id', 'store_name'));
        } else if ($tempId == 'REFUND_SUCCESS_CODE') {
            $order = app()->make(StoreRefundOrderRepository::class)->get($id);
            if (!$order || !$order->order->user_phone) return;
            $order_id = $order->order->order_sn;
            $store_name = $order->refundProduct[0]->product['cart_info']['product']['store_name'] . (count($order->refundProduct) ? '等' : '');
            self::create()->send($order->order->user_phone, $tempId, compact('order_id', 'store_name'));
        } else if ($tempId == 'REFUND_CONFORM_CODE') {
            $order = app()->make(StoreRefundOrderRepository::class)->get($id);
            if (!$order || !$order->order->user_phone) return;
            $order_id = $order->order->order_sn;
            $store_name = $order->refundProduct[0]->product['cart_info']['product']['store_name'] . (count($order->refundProduct) ? '等' : '');
            self::create()->send($order->order->user_phone, $tempId, compact('order_id', 'store_name'));
        } else if ($tempId == 'ADMIN_PAY_SUCCESS_CODE') {
            $order = app()->make(StoreGroupOrderRepository::class)->get($id);
            if (!$order) return;
            foreach ($order->orderList as $_order) {
                self::sendMerMessage($_order->mer_id, $tempId, [
                    'order_id' => $_order->order_sn
                ]);
            }
        } else if ($tempId == 'ADMIN_RETURN_GOODS_CODE') {
            $order = app()->make(StoreRefundOrderRepository::class)->get($id);
            if (!$order) return;
            self::sendMerMessage($order->mer_id, $tempId, [
                'order_id' => $order->refund_order_sn
            ]);
        } else if ($tempId == 'ADMIN_TAKE_DELIVERY_CODE') {
            $order = app()->make(StoreOrderRepository::class)->get($id);
            if (!$order) return;
            self::sendMerMessage($order->mer_id, $tempId, [
                'order_id' => $order->order_sn
            ]);
        } else if ($tempId == 'ADMIN_DELIVERY_CODE') {
            $order = app()->make(StoreOrderRepository::class)->get($id);
            if (!$order) return;
            self::sendMerMessage($order->mer_id, $tempId, [
                'order_id' => $order->order_sn
            ]);
        } else if ($tempId == 'BROADCAST_ROOM_CODE') {
            $room = app()->make(BroadcastRoomRepository::class)->get($id);
            if (!$room) return;
            self::create()->send($room->phone, $tempId, [
                'wechat' => $room->anchor_wechat,
                'date' => date('Y年m月d日 H时i分', strtotime($room->start_time))
            ]);
        } else if ($tempId == 'BROADCAST_ROOM_FAIL') {
            $room = app()->make(BroadcastRoomRepository::class)->get($id);
            if (!$room) return;
            self::create()->send($room->phone, $tempId, [
                'wechat' => $room->anchor_wechat
            ]);
        } else if ($tempId == 'PAY_PRESELL_CODE') {
            $order = app()->make(StoreOrderRepository::class)->get($id);
            if (!$order || !$order->user_phone || !$order->pay_time) return;
            self::create()->send($order->user_phone, $tempId, [
                'date' => date('Y-m-d', strtotime($order->pay_time)),
                'product_name' => $order->orderProduct[0]['cart_info']['product']['store_name'] ?? ''
            ]);
        } else if ($tempId == 'APPLY_MER_SUCCESS') {
            self::create()->send($id['phone'], $tempId, [
                'date' => date('m月d日', strtotime($id['date'])),
                'mer' => $id['mer'],
                'phone' => $id['phone'],
                'pwd' => $id['pwd'],
                'site_name' => systemConfig('site_name'),
            ]);
        } else if ($tempId == 'APPLY_MER_FAIL') {

            self::create()->send($id['phone'], $tempId, [
                'date' => date('m月d日', strtotime($id['date'])),
                'mer' => $id['mer'],
                'site' => systemConfig('site_name'),
            ]);
        } else if ($tempId == 'ARRIVAL_CODE') {

            $product = app()->make(ProductRepository::class)->getWhere(['product_id' => $id], '*', ['attrValue']);
            if (!$product) return false;
            $unique[] = 1;
            foreach ($product['attrValue'] as $item) {
                if ($item['stock'] > 0) {
                    $unique[] = $item['unique'];
                }
            }
            $make = app()->make(ProductTakeRepository::class);
            $query = $make->getSearch(['product_id' => $id, 'status' => 0, 'type' => 1])->where('unique', 'in', $unique);
            $tak_id = $query->column('product_take_id');
            $ret = $query->with(['user'])->select();
            app()->make(ProductTakeRepository::class)->updates($tak_id, ['status' => 1]);
            foreach ($ret as $item) {
                if ($item->user->phone) {
                    self::create()->send($item->user->phone, $tempId, [
                        'product' => $product->store_name,
                        'site' => systemConfig('site_name'),
                    ]);
                }
            }
        } else if ($tempId == 'INTEGRAL_INVALID') {

            self::create()->send($id['phone'], $tempId, [
                'integral' => $id['integral'],
                'date' => date('m月d日', strtotime($id['date'])),
                'site' => systemConfig('site_name'),
            ]);
        }
    }

    public static function sendMerMessage($merId, string $tempId, array $data)
    {
        $noticeServiceInfo = app()->make(StoreServiceRepository::class)->getNoticeServiceInfo($merId);
        $yunxinSmsService = self::create();
        foreach ($noticeServiceInfo as $service) {
            if (!$service['phone']) continue;
            $yunxinSmsService->send($service['phone'], $tempId, array_merge(['admin_name' => $service['nickname']], $data));
        }
    }

    /**
     * @return string
     * @author xaboy
     * @day 2020-05-18
     */
    protected function getToken()
    {
        return md5($this->config['sms_account'] . $this->config['sms_token']);
    }

    /**
     * @author xaboy
     * @day 2020-05-18
     */
    public function checkConfig()
    {
        if (!isset($this->config['sms_account']) || !$this->config['sms_account']) {
            throw new ValidateException('请登录短信账户');
        }
        if (!isset($this->config['sms_token']) || !$this->config['sms_token']) {
            throw new ValidateException('请登录短信账户');
        }
    }

    /**
     * 发送注册验证码
     * @param $phone
     * @return mixed
     */
    public function captcha($phone)
    {
        return json_decode(HttpService::getRequest(self::API . 'sms/captcha', compact('phone')), true);
    }

    /**
     * 短信注册
     * @param $account
     * @param $password
     * @param $url
     * @param $phone
     * @param $code
     * @param $sign
     * @return mixed
     */
    public function register($account, $password, $url, $phone, $code, $sign)
    {
        return $this->registerData(compact('account', 'password', 'url', 'phone', 'code', 'sign'));
    }

    /**
     * @param array $data
     * @return mixed
     * @author xaboy
     * @day 2020-05-18
     */
    public function registerData(array $data)
    {
        return json_decode(HttpService::postRequest(self::API . 'sms/register', $data), true);
    }

    /**
     * 公共短信模板列表
     * @param array $data
     * @return mixed
     */
    public function publictemp(array $data = [])
    {
        $this->checkConfig();
        $data['account'] = $this->config['sms_account'];
        $data['token'] = $this->config['sms_token'];
        $data['source'] = 'crmeb_merchant';
        return json_decode(HttpService::postRequest(self::API . 'sms/publictemp', $data), true);
    }

    /**
     * 公共短信模板添加
     * @param $id
     * @param $tempId
     * @return mixed
     */
    public function use($id, $tempId)
    {
        $this->checkConfig();
        $data = [
            'account' => $this->config['sms_account'],
            'token' => $this->config['sms_token'],
            'id' => $id,
            'tempId' => $tempId,
        ];

        return json_decode(HttpService::postRequest(self::API . 'sms/use', $data), true);
    }

    /**
     * @param string $templateId
     * @return mixed
     * @author xaboy
     * @day 2020-05-18
     */
    public function getTemplateCode(string $templateId)
    {
        return Config::get('sms.template_id.' . $templateId);
    }

    /**
     *  原 send 方法 （弃用）
     * 发送短信
     * @param string $phone
     * @param string $templateId
     * @param array $data
     * @return bool|string
     * @throws SmsException
     */
    public function sendDe(string $phone, string $templateId, array $data = [])
    {
        if (!$phone) {
            throw new SmsException('Mobile number cannot be empty');
        }

        $this->checkConfig();

        $formData['uid'] = $this->config['sms_account'];
        $formData['token'] = $this->config['sms_token'];
        $formData['mobile'] = $phone;
        $formData['template'] = $this->getTemplateCode($templateId);
        if (is_null($formData['template']))
            throw new SmsException('Missing template number');

        $formData['param'] = json_encode($data);
        $resource = json_decode(HttpService::postRequest(self::API . 'sms/send', $formData), true);
        if ($resource['status'] === 400) {
            throw new SmsException($resource['msg']);
        } else {
            app()->make(SmsRecordDao::class)->create([
                'uid' => $formData['uid'],
                'phone' => $phone,
                'content' => $resource['data']['content'],
                'template' => $resource['data']['template'],
                'record_id' => $resource['data']['id']
            ]);
        }
        return $resource;
    }

    /**
     * 账号信息
     * @return mixed
     */
    public function count()
    {
        $this->checkConfig();
        return json_decode(HttpService::postRequest(self::API . 'sms/userinfo', [
            'account' => $this->config['sms_account'],
            'token' => $this->config['sms_token']
        ]), true);
    }

    /**
     * 支付套餐
     * @param $page
     * @param $limit
     * @return mixed
     */
    public function meal($page, $limit)
    {
        return json_decode(HttpService::getRequest(self::API . 'sms/meal', [
            'page' => $page,
            'limit' => $limit
        ]), true);
    }

    /**
     * 支付码
     * @param $payType
     * @param $mealId
     * @param $price
     * @param $attach
     * @param $notify
     * @return mixed
     */
    public function pay($payType, $mealId, $price, $attach, $notify = null)
    {
        $this->checkConfig();
        $data['uid'] = $this->config['sms_account'];
        $data['token'] = $this->config['sms_token'];
        $data['payType'] = $payType;
        $data['mealId'] = $mealId;
        $data['notify'] = $notify ?? Route::buildUrl('SmsNotify')->build();
        $data['price'] = $price;
        $data['attach'] = $attach;
        return json_decode(HttpService::postRequest(self::API . 'sms/mealpay', $data), true);
    }

    /**
     * 申请模板消息
     * @param $title
     * @param $content
     * @param $type
     * @return mixed
     */
    public function apply($title, $content, $type)
    {
        $this->checkConfig();
        $data['account'] = $this->config['sms_account'];
        $data['token'] = $this->config['sms_token'];
        $data['title'] = $title;
        $data['content'] = $content;
        $data['type'] = $type;
        return json_decode(HttpService::postRequest(self::API . 'sms/apply', $data), true);
    }

    /**
     * 短信模板列表
     * @param $data
     * @return mixed
     */
    public function template(array $data)
    {
        $this->checkConfig();
        return json_decode(HttpService::postRequest(self::API . 'sms/template', $data + [
            'account' => $this->config['sms_account'], 'token' => $this->config['sms_token']
        ]), true);
    }

    /**
     * 获取短息记录状态
     * @param $record_id
     * @return mixed
     */
    public function getStatus(array $record_id)
    {
        return json_decode(HttpService::postRequest(self::API . 'sms/status', [
            'record_id' => json_encode($record_id)
        ]), true);
    }

    /**
     * @return YunxinSmsService
     * @author xaboy
     * @day 2020-05-18
     */
    public static function create()
    {
        /** @var ConfigValueRepository $make */
        $make = app()->make(ConfigValueRepository::class);
        $config = $make->more(['sms_account', 'sms_token'], 0);

        return new static($config);
    }

    /**
     * @param string $sms_account
     * @param string $sms_token
     * @return $this
     * @author xaboy
     * @day 2020-05-18
     */
    public function setConfig(string $sms_account, string $sms_token)
    {
        $this->config = compact('sms_token', 'sms_account');
        $this->config['sms_token'] = $this->getToken();
        return $this;
    }

    /**
     * @return Form
     * @throws FormBuilderException
     * @author xaboy
     * @day 2020-05-18
     */
    public function form()
    {
        return Elm::createForm(Route::buildUrl('smsCreate')->build(), [
            Elm::input('title', '模板名称'),
            Elm::input('content', '模板内容')->type('textarea'),
            Elm::radio('type', '模板类型', 1)->options([['label' => '验证码', 'value' => 1], ['label' => '通知', 'value' => 2], ['label' => '推广', 'value' => 3]])
        ])->setTitle('申请短信模板');
    }

    /**
     * @return mixed
     * @author xaboy
     * @day 2020-05-18
     */
    public function account()
    {
        $this->checkConfig();
        return $this->config['sms_account'];
    }

    /**
     * @Author:Qinii
     * @Date: 2020/9/19
     * @param $data
     * @return mixed
     */
    public function smsChange($data)
    {
        $this->checkConfig();
        $data['account'] = $this->config['sms_account'];
        $data['token'] = $this->config['sms_token'];
        return json_decode(HttpService::postRequest(self::API . 'sms/modify', $data), true);
    }

    /**
     * @Author:Qinii
     * @Date: 2020/9/19
     * @param $phone
     * @param $code
     * @param $type
     * @return bool
     */
    public function checkSmsCode($phone, $code, $type)
    {
        $sms_key = $this->sendSmsKey($phone, $type);
        if (!$cache_code = Cache::get($sms_key)) return false;
        if ($code != $cache_code) return false;
        Cache::delete($sms_key);
        return true;
    }

    /**
     * @Author:Qinii
     * @Date: 2020/9/19
     * @param $phone
     * @param string $type
     * @return string
     */
    public function sendSmsKey($phone, $type = 'login')
    {
        switch ($type) {
            case 'login': //登录
                return 'api_login_' . $phone;
                break;
            case 'binding': //绑定手机号
                return 'api_binding_' . $phone;
                break;
            case 'intention': //申请入住
                return 'merchant_intention_' . $phone;
                break;
            case 'change_pwd': //修改密码
                return 'change_pwd_' . $phone;
                break;
            case 'change_phone': //修改手机号
                return 'change_phone_' . $phone;
                break;
            default:
                return 'crmeb_' . $phone;
                break;
        }
    }

    public function send(string $phone, string $templateId, array $data = [])
    {
        try {
            $make = app()->make(CrmebServeServices::class)->sms();
            $resource = $make->send($phone,  $this->getTemplateCode($templateId), $data);
            if ($resource) {
                app()->make(SmsRecordDao::class)->create([
                    'uid' => $this->config['sms_account'],
                    'phone' => $phone,
                    'content' => $resource['content'],
                    'template' => $resource['template'],
                    'record_id' => $resource['id'],
                ]);
            }
        } catch (\Exception $exception) {
            throw new SmsException($exception->getMessage());
        }
    }
}
