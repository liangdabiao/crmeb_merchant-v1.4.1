<?php

// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------


namespace crmeb\services;


use app\common\model\store\order\StoreOrder;
use app\common\repositories\store\shipping\ExpressRepository;
use think\exception\ValidateException;
use think\facade\Cache;

class ExpressService
{
    const API = 'https://wuliu.market.alicloudapi.com/kdi';

    public static function query($no, $type = '')
    {
        $express = systemConfig('crmeb_serve_express');

        if($express == 2){
            //一号通
            return self::serve($no, $type);
        } else {
            //阿里云
            return self::ali($no, $type);
        }
    }

    /**
     * TODO 一号通查询
     * @param $no
     * @param $type
     * @return array
     * @author Qinii
     * @day 8/28/21
     */
    public static function serve($no,$type)
    {
        $res = app()->make(CrmebServeServices::class)->express()->query($no,$type);
        $cacheTime = 0;
        if(!empty($res)){
            if($res['status'] == 3){
                $cacheTime = 0;
            }else{
                $cacheTime = 1800;
            }
        }
        $list = $res['content'] ?? [];
        return compact('cacheTime','list');
    }

    /**
     * TODO 阿里云查询
     * @param $no
     * @param $re
     * @return array|bool
     * @author Qinii
     * @day 8/28/21
     */
    public static function ali($no, $re)
    {
        //阿里云
        $cacheTime = 0;
        $appCode = systemConfig('express_app_code');
        if (!$appCode) return false;
        $type = '';
        $res = HttpService::getRequest(self::API, compact('no', 'type'), ['Authorization:APPCODE ' . $appCode]);
        $result = json_decode($res, true) ?: null;
        if($result['status'] != 200){
            if (
                is_array($result) &&
                isset($result['result']) &&
                isset($result['result']['deliverystatus']) &&
                $result['result']['deliverystatus'] >= 3
            ){
                $cacheTime = 0;
            } else {
                $cacheTime = 1800;
            }
        }
        $list = $result['result']['list'] ?? [];
        return compact('cacheTime','list');
    }

    /**
     * TODO
     * @param $sn    快递号
     * @param $name  快递公司
     * @param $phone 收货人手机号
     * @return array|bool|mixed
     * @author Qinii
     * @day 8/16/21
     */
    public static function express($sn,$name,$phone)
    {
        if (Cache::has('express_' . $sn)) {
            $result = Cache::get('express_' . $sn);
        } else {
            $suffix = '';
            $is_shunfeng  = strtoupper(substr($sn,0,2));
            if  ($is_shunfeng ==  'SF') {
                $suffix = ':'.substr($phone,7);
            }
            $com = app()->make(ExpressRepository::class)->getSearch(['name' => $name])->value('code');
            $result = self::query($sn.$suffix, $com);
            if(!empty($result)){
                Cache::set('express_' . $sn, $result['list'], $result['cacheTime']);
                $result  =  $result['list'];
            }
        }
        return $result ?? [];
    }


}
