// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------
import Layout from '@/layout'
import { roterPre } from '@/settings'
const settingRouter =
    {
      path: `${roterPre}/setting`,
      name: 'setting',
      meta: {
        icon: 'dashboard',
        title: '权限管理'
      },
      alwaysShow: true,
      component: Layout,
      children: [
        {
          path: 'menu',
          name: 'setting_menu',
          meta: {
            title: '菜单管理'
          },
          component: () => import('@/views/setting/systemMenu/index')
        },
        {
          path: 'systemRole',
          name: 'setting_role',
          meta: {
            title: '身份管理'
          },
          component: () => import('@/views/setting/systemRole/index')
        },
        {
          path: 'systemAdmin',
          name: 'setting_systemAdmin',
          meta: {
            title: '管理员管理'
          },
          component: () => import('@/views/setting/systemAdmin/index')
        },
        {
          path: 'systemLog',
          name: 'setting_systemLog',
          meta: {
            title: '操作日志'
          },
          component: () => import('@/views/setting/systemLog/index')
        },
        {
            path: 'sms/sms_config/index',
            name: 'smsConfig',
            meta: {
              title: '一号通账户'
            },
            component: () => import('@/views/notify/smsConfig/index')
          },
          {
            path: 'sms/sms_template_apply/index',
            name: 'smsTemplate',
            meta: {
              title: '短信模板'
            },
            component: () => import('@/views/notify/smsTemplateApply/index')
          },
          {
            path: 'sms/sms_pay/index',
            name: 'smsPay',
            meta: {
              title: '套餐购买'
            },
            component: () => import('@/views/notify/smsPay/index')
          },
          {
            path: 'sms/sms_template_apply/commons',
            name: 'smsCommons',
            meta: {
              title: '公共短信模板'
            },
            component: () => import('@/views/notify/smsTemplateApply/index')
          },
          {
            path: 'sms/sms_config/config',
            name: 'smsConfig',
            meta: {
              title: '一号通配置',
              noCache: true
            },
            component: () => import('@/views/notify/smsConfig/config')
          },
          {
            path: 'notification/index',
            name: 'Notification',
            meta: {
              title: '一号消息管理通配置',
              noCache: true
            },
            component: () => import('@/views/system/notification/index')
          }          
      ]
    }

export default settingRouter
