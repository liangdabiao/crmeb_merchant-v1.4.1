// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------
import request from './request'

/**
 * @description 订单 -- 列表
 */
export function orderListApi(data) {
  return request.get('order/lst', data)
}

/**
 * @description 订单 -- 表头
 */
export function chartApi() {
  return request.get('order/chart')
}

/**
 * @description 订单 -- 卡片
 */
export function cardListApi(data) {
    return request.get('order/title', data)
}
/**
 * @description 订单 -- 编辑
 */
export function orderUpdateApi(id) {
  return request.get(`store/order/update/${id}/form`)
}

/**
 * @description 订单 -- 发货
 */
export function orderDeliveryApi(id) {
  return request.get(`store/order/delivery/${id}/form`)
}

/**
 * @description 订单 -- 详情
 */
export function orderDetailApi(id) {
  return request.get(`order/detail/${id}`)
}

/**
 * @description 退款订单 -- 列表
 */
export function refundorderListApi(data) {
  return request.get('order/refund/lst', data)
}

/**
 * @description 获取物流信息
 */
export function getExpress(id) {
  return request.get(`order/express/${id}`)
}
/**
 * @description 导出订单
 */
export function exportOrderApi(data) {
  return request.get(`order/excel`,  data )
}
/**
 * @description 导出退款单
 */
 export function exportRefundOrderApi(data) {
  return request.get(`order/refund/excel`,  data )
}
/**
 * @description 导出文件列表
 */
export function exportFileLstApi(data) {
  return request.get(`excel/lst`, data)
}

/**
 * @description 下载
 */
export function downloadFileApi(id) {
  return request.get(`excel/download/${id}`)
}
/**
 * @description 核销订单 -- 表头
 */
export function takeChartApi() {
  return request.get('order/takechart')
}
/**
 * @description 核销订单 -- 列表
 */
export function takeOrderListApi(data) {
  return request.get('order/takelst', data)
}
/**
 * @description 核销订单 -- 卡片
 */
 export function takeCardListApi(data) {
    return request.get('order/take_title', data)
}
/**
 * @description 导出列表 -- 文件类型
 */
 export function excelFileType() {
    return request.get('excel/type')
}

/**
 * @description Foxpur订单 -- 列表
 */
export function getFoxPurListApi(data) {
  return request.get('order/getfoxpurList', data)
}

/**
 * @description Foxpur导出订单
 */
export function exportFoxPurOrderApi(data) {
  return request.get(`order/excelfoxpur`,  data )
}

/**
 * @description Foxpur 候鸟导出订单
 */
export function exportFoxPurHouNiaoOrderApi(data) {
  return request.get(`order/excelFoxPurHouniao`,  data )
}

/**
 * @description Foxpur生成发货单
 */
export function exportInvoiceApi(data) {
  return request.get(`order/foxpurdelivery_export`, data)
}

/**
 * @description Foxpur订单核销
 */
export function orderCancellationApi(code) {
  return request.post(`order/verify/${code}`)
}


/**
 * @description Foxpur批量发货记录 -- 列表
 */
export function deliveryRecordListApi(data) {
  return request.get('store/import/lst', data)
}
/**
 * @description Foxpur批量发货记录 -- 详情
 */
export function deliveryRecordDetailApi(id, data) {
  return request.get(`store/import/detail/${id}`, data)
}
/**
 * @description Foxpur批量发货记录 -- 导出
 */
export function deliveryRecordImportApi(id) {
  return request.get(`store/import/excel/${id}`)
}

/**
 * @description Foxpur订单 -- 卡片
 */
export function cardFoxpurListApi(data) {
  return request.get('order/foxpurtitle', data)
}

/**
 * @description Foxpur订单 -- 批量下单状态
 */
export function batchChangeGroupFoxPurApi(data) {
  return request.get('order/batch_change_group_foxpur/form', data)
}

